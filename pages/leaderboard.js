import { useState } from 'react';
import React from 'react';
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    Stack
} from '@chakra-ui/react'
import arrowDown from "./../public/vectorarrowDown.png"

import Header from '../components/Header';
import SideBar from '../components/SideBar';
import ChallengeTable from '../components/tables/ChallengeTable';

export default function Leaderboard() {

    const getChallengeScores = async(challengeId) => {
        try {
            const response = await axios({
              method: "get",
              url: `https://api.manyactive.com/v1/saas/leaderboard/exercise/surrounding/${challengeId}`,
              headers: {
                // "Content-Type": "application/json",
                Authorization: `${user}`,
              },
            });

          } catch (err) {
            console.log(err);
          }
    }
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
            justifyContent="space-between"
		>
			{/*Sidebar Column */}
			<SideBar />
            {/* Main Column */}
                 
            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
                ml="auto"
                overflowY="scroll"
			>
				{/* Header */}
				<Header />

				{/* Top Row */}
				<Flex justifyContent="space-between" flexDir="row" align="center" mt={5}>
					<Flex align="center" flexDir="row">
						<Flex>
							<Heading as="h2" size="sm" letterSpacing="tight">Top 5</Heading>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                This week
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                                </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                Challenge
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                            </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                Department
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                            </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                Team
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                            </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                Country
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                            </Button>
						</Flex>
                        <Flex>
                            <InputGroup size="sm" bg="#001014">
                                {/* <InputLeftElement
                                pointerEvents="none" children={<FiSearch color="gray.300" />}
                                /> */}
                                <Input type="text" placeholder="Search" />
                            </InputGroup>
                        </Flex>
					</Flex>
					<Flex>
						<Link color="teal">View all</Link>
					</Flex>
				</Flex>

                <Flex justifyContent="space-between" mt={2} flexDir="row">
                    <Flex className="card" bg="#00313D" borderRadius={5} w="16%" flexDir="column" align="center" p={5}>
                        <Flex flexDir="row" justifyContent="space-between">
                            <Icon />
                            <Avatar size="xl" />
                            <Text>1<sup>st</sup></Text>
                        </Flex>
                        <Flex flexDir="column" align="center" mt={3}>
                            <Text>James Brown</Text>
                            <Text>(Trainer)</Text>
                            <Text fontWeight="bold">2900 <sub>steps</sub></Text>
                        </Flex>
                    </Flex>
                    <Flex className="card" bg="#00313D" borderRadius={5} w="16%" flexDir="column" align="center" p={5}>
                        <Flex flexDir="row" justifyContent="space-between">
                            <Avatar size="xl" />
                            <Text>2<sup>nd</sup></Text>
                        </Flex>
                        <Flex flexDir="column" align="center" mt={3}>
                            <Text>James Brown</Text>
                            <Text>(Trainer)</Text>
                            <Text fontWeight="bold">2900 <sub>steps</sub></Text>
                        </Flex>
                    </Flex>
                    <Flex className="card" bg="#00313D" borderRadius={5} w="16%" flexDir="column" align="center" p={5}>
                        <Flex flexDir="row" justifyContent="space-between">
                            <Avatar size="xl" />
                            <Text>3<sup>rd</sup></Text>
                        </Flex>
                        <Flex flexDir="column" align="center" mt={3}>
                            <Text>James Brown</Text>
                            <Text>(Trainer)</Text>
                            <Text fontWeight="bold">2900 <sub>steps</sub></Text>
                        </Flex>
                    </Flex>
                    <Flex className="card" bg="#00313D" borderRadius={5} w="16%" flexDir="column" align="center" p={5}>
                        <Flex flexDir="row" justifyContent="space-between">
                            <Avatar size="xl" />
                            <Text>4<sup>th</sup></Text>
                        </Flex>
                        <Flex flexDir="column" align="center" mt={3}>
                            <Text>James Brown</Text>
                            <Text>(Trainer)</Text>
                            <Text fontWeight="bold">2900 <sub>steps</sub></Text>
                        </Flex>
                    </Flex>
                    <Flex className="card" bg="#00313D" borderRadius={5} w="16%" flexDir="column" align="center" p={5}>
                        <Flex flexDir="row" justifyContent="space-between">
                            <Avatar size="xl" />
                            <Text>5<sup>th</sup></Text>
                        </Flex>
                        <Flex flexDir="column" align="center" mt={3}>
                            <Text>James Brown</Text>
                            <Text>(Trainer)</Text>
                            <Text fontWeight="bold">2900 <sub>steps</sub></Text>
                        </Flex>
                    </Flex>
                </Flex>
                {/* TABLE ROW */}
                <Flex flexDir="row" mt={2}>
                    <ChallengeTable />
                </Flex>
            </Flex>
		</Flex>
	)
}
