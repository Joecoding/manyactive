import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  datasets: [
    {
      label: 'Wallet Statement',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      backgroundColor: "#00C1F2",
      borderColor: "#00C1F2",
      borderWidth: 2,
      borderRadius: Number.MAX_VALUE,
      borderSkipped: false,
      barPercentage: 0.2
    },
  ],
};

const options = {
  maintainAspectRatio: false,
  scales: {

      x: {
          grid: {
              borderDash: [1, 1],
          }
      },
      y: {
          grid: {
              display: false,
          }
      },
  },
  plugins: {
      legend: {
          display: false,
      }
  }
};

const WalletChart = () => (
    <Bar 
        data={data} 
        options={options} 
    />
);

export default WalletChart;