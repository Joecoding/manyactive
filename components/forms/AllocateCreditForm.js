import React from 'react';
import {
    Button,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
    Flex,
    Heading,
    Input,
    Label,
    Select,
    NumberInput,
    NumberInputField,
    Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
    Lorem,
    useDisclosure,
    Text
  } from "@chakra-ui/react"

  export default function AllocateCreditForm() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    return (
      <>
        <Button bg="#006886" border="none" onClick={onOpen}>Allocate Credit</Button>
  
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent w="30 %" bg="#00313D" color="#fff" p="2%">
            <ModalHeader textAlign="center" mt={7}>Allocate Credit</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Text textAlign="center">You can allocate credit to your staff based on their job roles</Text>
                <FormControl mb={5}>
                  <FormLabel fontSize="xs">Job Role</FormLabel>
                  <Select variant="outline" placeholder="Manager">
                    <option value="#">Marketing</option>
                    <option value="#">Human Resources</option>
                  </Select>
                </FormControl>
                <FormControl mb={5}>
                  <FormLabel fontSize="xs">Credit Allocated</FormLabel>
                  <Input type="number" />
                </FormControl>
                <FormControl mb={5}>
                <FormLabel fontSize="xs">Available Categories</FormLabel>
                <Input type="number" />
                </FormControl>

                <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                Allocate
              </Button>
            </ModalBody>
          </ModalContent>
        </Modal>
      </>
    )
  }

// export default function AllocateCreditForm() {
//     return (
//         <Flex className="credit-form">
//             <FormControl id="job-role">
//                 <FormLabel>Job Role</FormLabel>
//                 <Select placeholder="Manager">
//                     <option>Owner</option>
//                     <option>Sales</option>
//                 </Select>
//             </FormControl>
//             <FormControl id="credit">
//                 <FormLabel>Credit Allocated</FormLabel>
//                 <NumberInput min={1}>
//                 <NumberInputField placeholder="200" />
//                 </NumberInput>
//             </FormControl>
//             <FormControl id="categories">
//                 <FormLabel>Available Categories</FormLabel>
//                 <Select placeholder="Spa, Gym">
//                     <option>#</option>
//                     <option>#</option>
//                 </Select>
//             </FormControl>
//             <Button type="submit">
//                 Allocate
//             </Button>
//         </Flex>
//     )
// } 