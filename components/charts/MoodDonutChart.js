import React from 'react';
import { Doughnut } from 'react-chartjs-2';

const data = {
  labels: ['Rad', 'Good', 'Meh', 'Fugly', 'Awful'],
  datasets: [
    {
      data: [60, 80, 50, 20, 16],
      backgroundColor: ["#FE503F", "#34A853", "#00C1F2", "#FC8936", "#BDFC36"],
      borderColor: "#00C1F2",
      borderWidth: 0,
      borderRadius: "50%",
      borderSkipped: false,
      barPercentage: 0.5,
      cutout: '90%',
      scaleShowLabels: false, 
    },
  ],
};



const counter = {
    id: 'counter',
    beforeDraw( chart, args, options) {
        const { ctx, chartArea: { top, right, bottom, left, width, height }} = chart;
        ctx.save();
        ctx.fillStyle = 'blue';
        ctx.fillRect(335, 335, 15, 15);
        // x0 = starting point on the horizontal level
        // y0 = starting point on the vertical level
        //x1 = length of the shape in pixel horizontal level
        //y1 = lenth of the shape in pixel vertical level
    }
};

const options = {
  maintainAspectRatio: false,
  plugins: {
      legend: {
          display: true,
          position: 'bottom',
          labels: {
            usePointStyle: true,
            boxWidth: 100,
            
          }
      }
  },
  scaleShowLabels: false
};




const MoodDonutChart = () => (
    <Doughnut 
        data={data}
        options={options}
    />
)


export default MoodDonutChart;