import React, { useState } from 'react'
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuItemOption,
    MenuGroup,
    MenuOptionGroup,
    MenuIcon,
    MenuCommand,
    MenuDivider
} from '@chakra-ui/react'

import Header from '../components/Header';
import SideBar from '../components/SideBar';
import MoodBarChart from '../components/charts/MoodBarChart';
import MoodDonutChart from '../components/charts/MoodDonutChart';
import arrowDown from "./../public/vectorarrowDown.png"
import vectorExport from "./../public/vectorExport.png"

export default function Moodboard() {
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
            justifyContent="space-between"
		>
            <SideBar />
            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
                ml="auto"
                overflowY="scroll"
			>
				{/* Header */}
				<Header />

				{/* Top Row */}
				<Flex flexDir="row" minH="40vh"
                        bg="#00313D" borderRadius={10}>
                    <Flex 
                        flexDir="column"
                        w="35%">
                            <Flex>
                                <MoodDonutChart />
                            </Flex>
                            <Flex>
                                
                            </Flex>
                            
                    </Flex>

                    <Flex 
                        flexDir="column"
                        w="65%">
                            <MoodBarChart />
                        </Flex>
                </Flex>
                <Flex justifyContent="space-between" flexDir="row" align="center">
					<Flex align="center" flexDir="row" width="60%">
						<Flex>
							<Heading as="h2" size="sm" letterSpacing="tight">Staff Moodboard</Heading>
						</Flex>
						<Flex>
                        <Menu>
                            <MenuButton variant="ghost" as={Button} size="md" display="flex" justifyContent="space-between">
                                <Flex alignItems="center">
                                    <span>
                                    Today
                                    </span>
                                    <span borderRadius="50%">
                                    <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100"  />
                                    </span>
                                </Flex>
                            </MenuButton>
                                <MenuList color="black">
                                    <MenuItem>Today</MenuItem>
                                    <MenuItem>This Week</MenuItem>
                                    <MenuItem>This Month</MenuItem>
                                </MenuList>
                        </Menu>
						</Flex>
						<Flex>
							<Button className="button" variant="ghost">
                            <img className="vector-export" width="15" height="10" src={vectorExport} alt="export-icon" quality="100" />
                                Export</Button>
						</Flex>
					</Flex>
					<Flex>
						<Link color="teal">View all</Link>
					</Flex>
				</Flex>
            </Flex>
        </Flex>
    )
}