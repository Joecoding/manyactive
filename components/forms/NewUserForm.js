import React, {useState,useEffect} from 'react';
import axios from 'axios';
import {
    Avatar,
    Button,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
    Flex,
    Heading,
    Input,
    Icon,
    Label,
    Select,
    NumberInput,
    NumberInputField,
    Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
    Lorem,
    useDisclosure,
    Radio,
    RadioGroup,
    Spacer,
    Text
  } from "@chakra-ui/react"
  import {
    FiUser,
    FiBell,
    FiUserPlus
} from "react-icons/fi";
import { FaRunning } from 'react-icons/fa';
import { GiBootPrints, GiBoots } from 'react-icons/gi';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'; 
import { useRouter } from "next/router";
import { applyAuth } from '../../utils/auth';


export default function NewUserForm() {
    const router = useRouter();
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [authuser, setAuthUser] = useState({});

    const [user, setUser] = useState({
        email: "",
        firstName: "",
        lastName: "",
        phone: "",
       })

    useEffect(() => {
        applyAuth(router, setAuthUser);
      }, [authuser, router]);
    

    const  popup = (message) => {  
      onClose();
      confirmAlert({
        title: 'Message',
        message: message,
        buttons: [
          {
            label: 'Ok',
            onClick: () => {}
          }
        ]
      });
    };

    const handleSubmit = async (e) => {
      e.preventDefault();    
      const postUserFormData = new FormData();   
      postUserFormData.append("userEmail", user.email);
      postUserFormData.append("userFirstName", user.firstName);
      postUserFormData.append("userLastName", user.lastName);
      postUserFormData.append("userPhone", user.phone);
      
      console.log(postUserFormData);
      try {
        // axios.defaults.headers.common = { Authorization: `${user}` }
        const response = await axios({
          method: "post",
          url: "https://api.manyactive.com/v1/saas/orgInvite",
          data: {
            email: user.email,
            firstname: user.firstName,
            lastname: user.lastName,
            phone: user.phone,
           
          },
          headers: { 
              "Content-Type": "application/json",
              "Authorization":  `${authuser}`
            },
        });
        popup('User successfully Invited.');
        console.log(response);
      }catch(error) {
        popup('Failed to add new user. Please contact admin.');
        console.log(error);
      }
    };

    const handleChange = (e) => {
        e.preventDefault();
        setUser({
          ...user,
          [e.target.name]: e.target.value,
        });
      };
    
    return (
      <>
        <Button size="sm" className="button" bg="#00C1F2" border="none" onClick={onOpen} leftIcon={<FiUserPlus />} variant="solid">Add User(s)</Button> 
        <Modal size="sm" isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <form onSubmit= {handleSubmit} >
          <ModalContent bg="#00313D" color="#fff" p="2%">
            <ModalHeader fontSize="sm" fontWeight="normal">User Details</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>
                <Flex flexDir="row" align="center" mb={3}>
                    <Icon borderRadius="50%" boxSize={10} bg="black" as={GiBootPrints} />
                </Flex>
                <Flex flexDir="row">
                    <Flex flexDir="column">
                        <FormControl mb={5}>
                            <FormLabel fontSize="xs"  htmlFor="email" fontWeight="light">User Email</FormLabel>
                            <Input type="email" variant="outline" name="email"  id="email" value={user.email} onChange={handleChange} placeholder="eg. userDetailsmanyactive@gmail.com" />                              
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="firstName">First Name</FormLabel>
                            <Input type="text"  onChange={handleChange} value={user.firstName} id="firstName" name="firstName" placeholder="First Name" />
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="lastName">Last Name</FormLabel>
                            <Input type="text"  onChange={handleChange} value={user.lastName} id="lastName" name="lastName" placeholder="Last Name" />
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="phone">Phone Number</FormLabel>
                            <Input type="text" id="phone" name="phone" value={user.phone} onChange={handleChange} placeholder="Phone number" />
                        </FormControl>
                        
                    </Flex>
                    
                   
 
                </Flex>
                <Flex justifyContent="space-between" mt={3}>
                    <Button onClick={onClose} variant="ghost">Cancel</Button>
                    <Button type="submit" bg="#00C1F2">Add</Button>
                </Flex>
            </ModalBody>
          </ModalContent>
          </form>
        </Modal>
      </>
    )
  }
