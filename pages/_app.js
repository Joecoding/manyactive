import { ChakraProvider } from '@chakra-ui/react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import '@fontsource/raleway/400.css'
import '@fontsource/open-sans/700.css'

import theme from '../theme/index'

function MyApp({ Component, pageProps }) {
  
  return(
  <ChakraProvider theme={theme}>
    <Component {...pageProps} />
  </ChakraProvider>
  )
}

export default MyApp
