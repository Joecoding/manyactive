import React, { useEffect, useState } from "react";
import { Flex, Heading, Text, Icon, Button } from "@chakra-ui/react";
import { useRouter } from "next/router";
import Header from "../../components/Header";
import SideBar from "../../components/SideBar";
import { GiBootPrints } from "react-icons/gi";
import NewChallengeForm from "../../components/forms/NewChallengeForm";
import { applyAuth } from "../../utils/auth";

export default function NewChallenge() {
  const [user, setUser] = useState({})
  const router = useRouter()

  useEffect(() => {
    applyAuth(router, setUser);
  }, [user, router])

  return (
    <Flex h="100%" flexDir="row" overflow="hidden" maxW="100%" justifyContent="space-between">
      <SideBar />
      <Flex
        w="85%"
        p="2%"
        flexDir="column"
        minH="50vh"
        backgroundColor="#171717"
        color="#fff"
        
        ml="auto"
      >
        {/* Header */}
        <Header />
        <Flex flexDir="column" h="100%" mt={5}>
          <Flex flexDir="row">
            <Heading as="h2" size="md" fontWeight="semibold">
              Pre-made Challenges
            </Heading>
          </Flex>
          <Flex flexDir="row" justifyContent="space-between" mt={5}>
            <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  Steps Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <NewChallengeForm user={user}/>
              </Flex>
            </Flex>
            <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  Steps Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <NewChallengeForm user={user}/>
              </Flex>
            </Flex>
            <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  Steps Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <NewChallengeForm user={user}/>
              </Flex>
            </Flex>
          </Flex>
          <Flex flexDir="row" mt={2}>             
            <Heading as="h2" size="md" fontWeight="semibold">
              Created Challenges
            </Heading>
          </Flex>
          <Flex flexDir="row" justifyContent="space-between" mt={5}>
          <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  HR Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                Publish
              </Button>
              </Flex>
            </Flex>
          <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  HR Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                Publish
              </Button>
              </Flex>
            </Flex>
          <Flex p="2%">
              <Flex
                className="card"
                flexDir="column"
                bg="#00313D"
                borderRadius={5}
                h="260px"
                w="260px"
                p={5}
              >
                <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                <Heading mb={4} size="md" fontWeight="normal">
                  HR Challenge
                </Heading>
                <Text fontSize="sm" fontWeight="thin">
                  This challenge helps participants see how many steps they take
                  during the challenge period
                </Text>
                <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                Publish
              </Button>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
}
