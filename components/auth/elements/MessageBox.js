import React from 'react';
import {
    Alert,
    AlertIcon,
    CloseButton,
} from "@chakra-ui/react"

// props.children will show the contents of messagebox in the palce it has been used
function MessageBox(props){
  
    return(
        // <div className={`alert alert-${props.variant}` || 'info'}>{props.children}</div>
        props.alertHidden ? (
            <Alert  
        status={props.status}
        variant={props.variant}
        // flexDirection="column"
        alignItems="center"
        justifyContent="center"
        textAlign="center"        
        >
        <AlertIcon />
        {props.children} 
        <CloseButton aria-hidden="true" position="absolute" right="8px" top="8px" onClick={props.hideAlert}/>
        </Alert>
        ) : " "
        
    )
    
};

export default MessageBox;

