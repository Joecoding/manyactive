
import axios from 'axios';

export const getTeams =  async( user, setTeams ) => {
//Get Teams Function
    try{
        const response = await axios({
            method: "get",
            url: "https://api.manyactive.com/v1/saas/teams/",
            headers: { 
                "Content-Type": "application/json",
                "Authorization": `${user}`
            },
         });
         setTeams(response.data.teamsData);   
         console.log(response.data.teamsData)    
        }catch(err){
            console.log(err);
    }

}

//Get Members Function
export const getMembers =  async( user, setMembers ) => {

    try{
        const response = await axios({
            method: "get",
            url: "https://api.manyactive.com/v1/saas/members/",
            headers: { 
                // "Content-Type": "application/json",
                "Authorization":  `${user}`
            },
         });
         setMembers(response.data);       
        }catch(err){
            console.log(err);
    }

}


//Get Challenges Function
export const getChallenges =  async( user, setChallenges ) => {

    try{
        const response = await axios({
            method: "get",
            url: "https://api.manyactive.com/v1/saas/challenges/",
            headers: { 
                // "Content-Type": "application/json",
                "Authorization":  `${user}`
            },
         });
         setChallenges(response.data);       
        }catch(err){
            console.log(err);
    }

}