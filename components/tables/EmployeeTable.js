import React from 'react';
import { useRouter } from 'next/router';
import {
    Image,
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
    Stack,
    FiMoreVertical,
    ChakraProvider
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
    FiArrowDownCircle,
	FiExternalLink,
    FiArrowForwardCircle,
    FiArrowBackCircle

} from "react-icons/fi";


export default function EmployeeTable({ wallet, page, showCard = true}) {

    const router = useRouter();

    const employeeRecord = [

        { staffId : "2536", Name : "Balogun Joseph", Department: "Human Resource" , activityBooked: "Cardioblast", deductedCredit : "12000", creditBalance: " 5000", locationTime: "23/494949" },
        { staffId : "2536", Name : "Balogun Joseph", Department: "Human Resource" , activityBooked: "Cardioblast", deductedCredit : "12000", creditBalance: " 5000", locationTime: "23/494949" },
        { staffId : "2536", Name : "Balogun Joseph", Department: "Human Resource" , activityBooked: "Cardioblast", deductedCredit : "12000", creditBalance: " 5000", locationTime: "23/494949" },
        { staffId : "2536", Name : "Balogun Joseph", Department: "Human Resource" , activityBooked: "Cardioblast", deductedCredit : "12000", creditBalance: " 5000", locationTime: "23/494949" }

    ]
   
    return(
    <>
        <Flex flexDir="column" w="100%">
            <Flex overflow="auto">

                {showCard &&
                    
                        <Flex justifyContent="space-between" mt={2} flexDir="row">
                        {employeeRecord.map((n, i) => {
                        return(
                        <Flex className="card" bg="#00313D" borderRadius={10} w="170px" h="200px" flexDir="column" align="center" p={3}>
                            <Flex flexDir="row" justifyContent="space-between">
                                <Avatar size="xl" />
                                <Icon align="right" as={FiMoreVertical} />
                            </Flex>
                            <Flex flexDir="column" align="center" mt={3}>
                                <Text>{n.Name}</Text>
                                <Text fontSize="sm" fontWeight="light">{n.Department}</Text>
                                <Text fontSize="sm" fontWeight="light">(Trainer)</Text>
                            </Flex>
                        </Flex>
                        );
                        })}
                        </Flex>
                        
                }
                {!showCard && <Table variant="unstyled" mt={4}>
                    <Thead>
                        <Tr backgroundColor="#00313D">
                            <Th>Staff ID</Th>
                            <Th>Name</Th>
                            <Th>Department</Th>
                            <Th>Activity Booked</Th>
                            <Th isNumeric>Deducted credit</Th>
                            <Th isNumeric>Credit balance</Th>
                            <Th>Location/Time</Th>
                        </Tr>
                    </Thead>
                    <Tbody>

                        {employeeRecord.slice(0, 3).map((n, i) => {

                        return(
                        <Tr key={i}>
                            <Td>{n.staffId}</Td>
                            <Td>{n.Name}</Td>
                            <Td>{n.Department}</Td>
                            <Td>{n.activityBooked}</Td>
                            <Td isNumeric>{n.deductedCredit}</Td>
                            <Td isNumeric>{n.creditBalance}</Td>
                            <Td>{n.locationTime}</Td>
                        </Tr>
                        );
                        })}
                   
                   
                    </Tbody>
                    <tfoot>
                    <Flex flexDir="row" justifyContent="space-between" >
						<Flex>
							<Button  onClick={() => router.push(`/wallet?page=${page - 1 }`)} className="button" style={{ float: "left" }}  variant="ghost"> Previous </Button>
						</Flex>
						<Flex>
							<Button onClick={() => router.push(`/wallet?page=${page + 1 }`)}   className="button" style={{ float: "right" }}  variant="ghost"> Next </Button>
					  </Flex>
                    </Flex>


                    </tfoot>
                </Table>}
            
						
					
					

                </Flex>
              </Flex>
    </>
    )
}


export async function getServerSideProps({ query: {page: number = 1}}){

   const res = await fetch("localhost:5000/wallet?_limit=3");

   const data = await res.json();

  

   return {
     
    props: {
        wallet : employeeRecord,

        page: +page


    }

   }

}