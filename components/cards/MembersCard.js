import React from 'react';
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    VStack,
    Stack
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
	FiArrowDownCircle,
	FiExternalLink,
    FiMoreVertical,
    FiClock
} from "react-icons/fi";
import { GiBootPrints } from 'react-icons/gi'

export default function MembersCard() {
    return(        
        <Flex
            flexDir="column"
            justifyContent="space-between"  w="16%" bg="#00313D" h="" borderRadius={5} align="center" p={5}>
            
            <Flex flexDir="row" justifyContent="space-between">
                <Icon />
                <Avatar size="lg" />
                <Text>1<sup>st</sup></Text>
            </Flex>
            <Flex flexDir="column" align="center" mt={3}>
                <Text>James Brown</Text>
                <Text>(Trainer)</Text>
                <Text fontWeight="bold">2900 <sub>steps</sub></Text>
            </Flex>  
        </Flex>
    )
}