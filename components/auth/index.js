import React from "react";
import Head from "next/head";

function Layout({ title = "", description = "Many Active", children, style }) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow" />
        <meta name="description" content={description} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content={description} />
        <meta property="og:image:alt" content="Many Active For Men And Women" />
        <meta property="og:image:width" content="1253" />
        <meta property="og:image:height" content="740" />
        <meta property="og:locale" content="en_US" />
        <meta name="theme-color" content="#00C1F2" />
        <meta property="og:site_name" content={title} />
        <link rel="shortcut icon" href="/favicon.png" />
      </Head>

      <div className="wrapper" style={style}>{children}</div>
      <div id="modal-root"></div>
    </>
  );
}

export default Layout;
