import React from "react";

export default function Button({ secondary, children, className, ...props }) {
  let secondaryProp = secondary ? "secondary-btn" : null;
  return (
    <button
      {...props}
      className={`${secondaryProp}  ${className ? className : ""} button`}
    >
      {children}
    </button>
  );
}
