import React, { useEffect, useState } from "react";
import "../styles/Home.module.css";
//import Button from "../components/auth/elements/button";
import Head from "next/head";
import Link from "next/link";
import axios from "axios";
const siteTitle = "Manyactive";
import { useRouter } from "next/router";
import {
  Flex, 
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  InputGroup,
  InputRightElement,
  Button
} from "@chakra-ui/react";
import { Form } from "react-bootstrap";
import teams1 from "../public/teams1.png";
import MessageBox from "../components/auth/elements/MessageBox";

export default function Login() {
  const [formValue, setFormValue] = React.useState({
    email: "",
    password: "",
  });
  const [isError, setIsError] = useState(false)
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [loading, setLoading] = useState(true)

  const showPassword = () => setShow(!show);

  const hideAlert = () => {
   setAlertHidden(!alertHidden);
  };


  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const loginFormData = new FormData();
    loginFormData.append("username", formValue.email);
    loginFormData.append("password", formValue.password);

    console.log(loginFormData);
    try {
      const response = await axios({
        method: "post",
        url: "https://api.manyactive.com/v1/saas/base/auth/login",
        data: {
          email: formValue.email,
          password: formValue.password,
        },
        headers: { "Content-Type": "application/json" },
      });
      console.log(response);

      if (response.status === 200) {
        localStorage.setItem('AuthToken', `${response.data.token}`);
        localStorage.setItem('email', `${formValue.email}`);
        console.log("token", response.data.token)
        router.push("/teams");
        router.push("/");
        setLoading(false)



        
      const response2 = await axios({
        method: "get",
        url: "https://api.manyactive.com/v1/saas/org/currentAdmin",
        headers: { 
            "Content-Type": "application/json",
            "Authorization":  `${localStorage.getItem('AuthToken')}`
          },
      });
      
      }else{
        setIsError(true)
      }
    } catch (err) {
      setIsError(true)
      setError(err.message);
      console.log('err', error);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });    
  };

  
  return (    
    <Flex
    h="100vh"
    flexDir="row"
    overflow="hidden"
    maxW="2000px"
  >
          {/* Main Column */}

          <Flex
              w="100%"
              p="1rem"
              flexDir="column"
              minH="50vh"
      backgroundColor="#171717"
      color="#fff"
    >
          {/* Card Component */}
          <Flex className="card"
            w="50%"
            h="100%"
            flexDir="column"
            m='auto'
            backgroundColor="#00313D"
            p="3%"
            borderRadius={5}
          >
            {isError ?
             (<MessageBox status="error" variant="solid" alertHidden={alertHidden} isError={isError} hideAlert={hideAlert}>{`${error}.\n user details not correct.`}</MessageBox>) : ""}
                        <div className="m-auto">
            <div className="text-center">
              <h2 className="primary-color">SIGN IN</h2>
            </div>
            <div>
              <div className="form-body">
                <form onSubmit={handleSubmit} id="form" className="form mt-auto">
                <FormControl isInvalid={isError} className="p-2 pb-0">
                <FormLabel htmlFor="email">Email</FormLabel>
                <Input border="2px" type="email" id="email" placeholder="email" value={formValue.email} name="email" onChange={handleChange} />
                {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
              </FormControl>
              <FormControl isInvalid={isError} className="p-2 pb-0">
                  <FormLabel htmlFor="password">Password</FormLabel>
                  <InputGroup>
                  <Input border="2px" type={show ? "text" : "password"} id="password" placeholder="password" value={formValue.password} name="password" onChange={handleChange} />
                  {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
                  <InputRightElement width="4.5rem">
                  <Button colorScheme="teal" h="1.75rem" size="sm" onClick={showPassword}>
                    {show ? "Hide" : "Show"}
                  </Button>
                </InputRightElement>
                  </InputGroup>
              </FormControl>
              <Button 
              // isLoading 
              colorScheme="none" 
              // variant="outline" 
              type="submit" 
              className="w-100 mt-3" 
              onClick={hideAlert} size="md">
              Login
              </Button>
              <FormControl>
              <FormHelperText>
                Dont have a manyactive account?{" "}
                <Link href="/register">
                  <a className="primary-color">Sign up</a>
                </Link>
              </FormHelperText>
              </FormControl>
            </form>
            </div>
            </div>
            </div>
            </Flex>
           </Flex>
           </Flex>

  );
}

{/* <Form id="form" className="form mt-auto" onSubmit={handleSubmit}>
<Form.Group className="mb-1 p-3 pb-0" controlId="formBasicEmail">
  <Form.Control
    size="md"
    type="email"
    name="email"
    placeholder="Email"
    value={formValue.email}
    onChange={handleChange}
  />
</Form.Group>
<Form.Group className="mb-1 p-3 pb-0" controlId="formBasicPassword">
  <Form.Control
    size="md"
    type="password"
    name="password"
    placeholder="Password"
    value={formValue.password}
    onChange={handleChange}
  />
  </Form.Group>
  <Button
    size="md"
    secondary
    className="w-100 mt-3"
    variant="primary"
    type="submit"
  >
    Login
  </Button>
  <Form.Text>
    Dont have a manyactive account?{" "}
    <Link href="/register">
      <a className="primary-color">Sign up</a>
    </Link>
    
  </Form.Text>
</Form> */}