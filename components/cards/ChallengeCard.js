import React from 'react';
import Pagination from 'next-pagination'
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Button,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Grid,
    GridItem,
    Wrap,
    WrapItem
} from '@chakra-ui/react'
import {
    FiMoreVertical,
    FiClock,
    FiArrowRightCircle,
} from "react-icons/fi";
import { GiBootPrints } from 'react-icons/gi'
import moment from 'moment';
import TeamsToChallengeForm from '../forms/TeamsToChallengeForm';

export default function ChallengeCard({challenges, error}) {

    return(        
        <Wrap
        spacing='20px'
            overflow='hidden'
            >                    
                {/* <Flex
                    flexDir="row"
                    justifyContent="space-between" flexWrap="wrap"> */}
                    {
                        challenges ? (challenges.slice(0, challenges.length ).map((doc, i) => {
                    return(   
                        <WrapItem key={doc._id}>                
                        <Flex
                        className="card"
                        // flexDir="column"
                        bg="#00313D"
                        borderRadius={5}
                        h="280px"
                        w="260px"
                        p={2}
                        mt={2}                        
                      >
                        <Flex Flex flexDir="row"  justifyContent="flex-end" >
                              <TeamsToChallengeForm challengeId={doc._id} />
                        </Flex>
                        <Flex flexDir="column" mx={2}>
                            <Text mb={2} fontWeight="thin" fontSize="xs">{moment(doc.startDate).local().format('LLL')}</Text>
                            <Flex mb={3} align="center">
                                <Icon size="lg" color="#00C1F2" m={2} as={GiBootPrints} />
                                <Heading fontWeight="normal" letterSpacing="tight" ml={2} size="md">{doc.name}</Heading>
                            </Flex>
                            <Flex mb={5} flexDir="column">
                                <Heading fontWeight="normal" as="h5" size="xs">Description</Heading>
                                <Text as="p" fontSize="xs" letterSpacing="tight" fontWeight="thin">{doc.description}</Text>
                            </Flex>
                            <Flex mb={5} flexDir="row">
                                <Flex flexDir="column">
                                    <Heading fontWeight="normal" fontSize="xs">Challenge Type</Heading>
                                    <Text fontWeight="thin" fontSize="xs" letterSpacing="tight">{doc.type}</Text>
                                </Flex>
                                
                                <Flex flexDir="column" ml={4}>
                                    <Heading fontWeight="normal" fontSize="xs">Challenge ID</Heading>
                                    <Text fontWeight="thin" fontSize="xs" letterSpacing="tight">{doc._id}</Text>
                                </Flex>
                            </Flex>
                            <Flex align="center" mt={-1}>
                                <Icon color="#34A853" as={FiClock} />
                                <Text fontSize="xs" ml={1}>{doc.status}</Text>
                            </Flex>
                        </Flex>               
                    </Flex> 
                    </WrapItem>
                        ) })) : ""                    
                            }                         
                {/* </Flex> */}                     
        </Wrap>
    )
}
{/* <ChallengeToTeams /> */}