import React, { useState, useEffect, useCallback } from 'react'
import { useRouter } from "next/router";

import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
	Lorem,
	useDisclosure
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
	FiArrowDownCircle,
	FiExternalLink
} from "react-icons/fi";
import WalletChart from '../components/charts/WalletChart';
import Header from '../components/Header';
import EmployeeTable from '../components/tables/EmployeeTable';
import AllocateCreditForm from '../components/forms/AllocateCreditForm';
import TopUpCreditForm from '../components/forms/TopUpCreditForm';
import SideBar from '../components/SideBar';
import { applyAuth } from '../utils/auth';

import arrowDown from "./../public/vectorarrowDown.png"
import vectorExport from "./../public/vectorExport.png"

export default function Wallet() {
    const [user, setUser] = useState({});
	const { isOpen, onOpen, onClose } = useDisclosure();
	const [walletBalance, setWalletBalance] = useState(0);
    const [error, setError] = useState("");
    const router = useRouter();
    
    useEffect(() => {        
		applyAuth(router, setUser);
        getWalletBalance;
      }, [user, router]);


	const getWalletBalance = useCallback(async () => {    
        try {
          const response = await axios({
            method: "get",
            url: "https://api.manyactive.com/v1/transactions/wallet/balance",
            headers: { 
                "Content-Type": "application/json",
                "Authorization":  `${user}`
              },
          });
          setWalletBalance(response.data);
 
        }catch(err) {
          setError(err)
          console.log('err', error);
        };
      }, [user, error]);


	  
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
			justifyContent="space-between"
		>
			{/*Sidebar Column */}
			<SideBar />

            {/* Main Column */}

            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
				ml="auto"
				overflowY="scroll"
			>
				{/* Header */}
				<Header />
				
				
				{/* Top Row */}
				<Flex
					flexDir="row"
					minH="40vh"
					justifyContent="space-between"
					mt={3}>
						{/* Card Component */}
						<Flex className="card"
							w="45%"
							flexDir="column"
							backgroundColor="#00313D"
							p="3%"
							borderRadius={5}
						>
							<Flex flexDir="row" justifyContent="space-between">
								<Flex align="center">
									<Icon as={FiBarChart} />
									<Text>Credit Balance</Text>
								</Flex>
								<Flex align="center">
									<Icon size="lg" as={FiUsers} />
									<Text>Staff</Text>
								</Flex>
							</Flex>
							<Flex flexDir="row" justifyContent="space-between">
								<Text fontSize="4xl">{walletBalance}</Text>
								<Text fontSize="4xl">200</Text>
							</Flex>
							<Flex justifyContent="space-between" p={10} mt={4}>
								<TopUpCreditForm />
								<AllocateCreditForm />
							</Flex>
						</Flex>
						
						{/* Wallet Statement Chart */}
						<Flex className="chart"
							w="40%"
							flexDir="column"
							overflow="hidden"
						>
							<Flex justifyContent="space-between" align="center">
								<Text>Wallet Statement</Text>
								<Link color="teal">Generate Statement</Link>
							</Flex>
							
							<WalletChart />
						</Flex>			
				</Flex>				
				{/* Table Row */}				
				<Flex justifyContent="space-between" mt={4} flexDir="row" align="center">
					<Flex align="center" flexDir="row">
						<Flex>
							<Heading as="h2" size="sm" letterSpacing="tight">Staff Activities</Heading>
						</Flex>
						<Flex>
							<Button className="button" variant="ghost">
								This week
								<img className="arrow-down" width="15" height="10" src={arrowDown} alt="arrow-down" quality="100" />
							</Button>
						</Flex>
						<Flex>
							<Button className="button" 
							// leftIcon={<FiExternalLink />}
							 variant="ghost">
								<img className="vector-export" width="15" height="10" src={vectorExport} alt="export-icon" quality="100" />
								Export
							</Button>
						</Flex>
					</Flex>
					<Flex>
						<Link color="teal">View all</Link>
					</Flex>
				</Flex>
				<Flex flexDir="row">
				{/* TABLE  */}				
					<EmployeeTable />				
				</Flex>
            </Flex>
		</Flex>
	)
}
