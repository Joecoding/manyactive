import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import cookieCutter from 'cookie-cutter';
import {
    Flex,
    Heading,
    Icon,
} from '@chakra-ui/react'
import {
    FiUser,
    FiUsers,
    FiBarChart2,
    FiFlag,
    FiSmile,
    FiSettings,
    FiMessageSquare
} from "react-icons/fi";

import { BiSupport, BiWalletAlt } from "react-icons/bi";
import { FaRunning } from 'react-icons/fa'
import { RiLogoutCircleLine } from 'react-icons/ri';
import vectorWallet from "./../public/vectorWallet.svg";


export default function SideBar() {
    const router = useRouter();
    const [user, setUser] = useState("")

    const logout = () => {
        localStorage.setItem('AuthToken', "");
        router.push("/login");
    }
    return(
            <Flex
                w="15%"
                flexDir="column"
                alignItems="center"
                backgroundColor="#00313D"
                color="#fff"
                // overflow="hidden"
                position="fixed"
                h="100%"
            >
                <Flex
                    flexDir="column"
                    justifyContent="space-between"
                >
                    <Flex
                        flexDir="column"
                        as="nav"
                    >
                        <Heading
                            mt={5}
                            mb={10}
                            fontSize="2xl"
                            alignSelf="center"
                            letterSpacing="tight"
                        >
                            many<span>active&gt;&gt;&gt;</span>
                        </Heading>
                        <Flex
                            flexDir="column" align="flext-start" justifyContent="center">
                            <Heading ml="30px" mb={3} size="xs" fontWeight="normal">MAIN</Heading>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/" ? "active-icon" : ""} as={FiBarChart2} fontSize="lg" mr={2} />
                                <Link href="/">
                                    <a className={router.pathname == "/" ? "active" : ""}>Home</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/members" ? "active-icon" : ""} as={FiUser} fontSize="lg" mr={2} />
                                <Link href="/members">
                                    <a className={router.pathname == "/members" ? "active" : ""}>Members</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                            
                                <Icon className={router.pathname == "/wallet" ? "active-icon" : ""} as={BiWalletAlt} fontSize="lg" mr={2} />
                                <Link href="/wallet">
                                    <a className={router.pathname == "/wallet" ? "active" : ""}>
                                    {/* <Flex> */}
                                    {/* <img className="sidebar-img" width="14" height="14" src={vectorWallet} alt="arrow-down" quality="100" /> */}
                                    Wallet
                                    {/* </Flex> */}
                                    </a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/challenge" || router.pathname=="/challenge/new" ? "active-icon" : ""} as={FiFlag} fontSize="lg" mr={2} />
                                <Link href="/challenge/"> 
                                    <a className={router.pathname == "/challenge" || router.pathname=="/challenge/new" ? "active" : ""}>Challenge</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/teams" ? "active-icon" : ""} as={FiUsers} fontSize="lg" mr={2} />
                                <Link href="/teams">
                                    <a className={router.pathname == "/teams" ? "active" : ""}>Teams</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/leaderboard" ? "active-icon" : ""} as={FaRunning} fontSize="lg" mr={2} />
                                <Link  href="/leaderboard">
                                    <a className={router.pathname == "/leaderboard" ? "active" : ""}>Leaderboard</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/moodboard" ? "active-icon" : ""} as={FiSmile} fontSize="lg" mr={2} />
                                <Link href="/moodboard">
                                    <a className={router.pathname == "/moodboard" ? "active" : ""}>Moodboard</a>
                                </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon className={router.pathname == "/messages" ? "active-icon" : ""} as={FiMessageSquare} fontSize="lg" mr={2} />
                                <Link href="/messages">
                                    <a className={router.pathname == "/messages" ? "active" : ""}>Messages</a>
                                </Link>
                            </Flex>
                            
                        </Flex>

                        <Flex
                            flexDir="column" align="flext-start" justifyContent="center">
                            <Heading fontWeight="normal" size="xs" mt={10} mb={3} ml="30px">GENERAL</Heading>
                            <Flex className="sidebar-items">
                                <Icon as={FiSettings} fontSize="lg" mr={2} />
                                <Link href="#">
                                    <a>Settings</a>
                               </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon as={BiSupport} fontSize="lg" mr={2} />
                                <Link href="#">
                                    <a>Help</a>
                               </Link>
                            </Flex>
                            <Flex className="sidebar-items">
                                <Icon as={RiLogoutCircleLine} fontSize="lg" mr={2} />
                                <Link href="#">
                                    <a onClick={logout}>Logout</a>
                                </Link>
                            </Flex>
                        </Flex>
                    </Flex>
                </Flex>
            </Flex>
    )
}
