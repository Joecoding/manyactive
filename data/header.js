export const navData = [
    {
        name: "Home",
        key: "home",
        to: ""
    },
    {
        name: "Business",
        key: "business",
        to: "business"
    },
    {
        name: "Teams",
        key: "teams",
        to: "teams"
    },
    {
        name: "Stories",
        key: "stories",
        to: "stories"
    },
    {
        name: "Contact us",
        key: "contact",
        to: "mailto:support@manyactive.co.ke"
    },
    {
        name: "Login",
        key: "login",
        to: "register",
        button: true
    },
];

