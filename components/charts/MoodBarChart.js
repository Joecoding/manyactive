import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: ['Rad', 'Good', 'Meh', 'Fugly', 'Awful'],
  datasets: [
    {
      data: [60, 80, 50, 20, 16],
      backgroundColor: ["#FE503F", "#34A853", "#00C1F2", "#FC8936", "#BDFC36"],
      borderColor: "#00C1F2",
      borderWidth: 0,
      borderRadius: Number.MAX_VALUE,
      borderSkipped: false,
      barPercentage: 0.5,
      scaleShowLabels: false, 
    },
  ],
};

const options = {
  plugins: {
      legend: {
          display: false,
      }
  },
  scaleShowLabels: false
};


const MoodBarChart = () => (
    <Bar
        data={data} 
        options={options} 
    />
);




export default MoodBarChart;