import React, { useState } from 'react'
import { FlutterWaveButton, closePaymentModal } from 'flutterwave-react-v3';

import {
    Button,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
    Flex,
    Heading,
    Input,
    Label,
    Select,
    NumberInput,
    NumberInputField,
    Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
    Lorem,
    useDisclosure,
    Text
  } from "@chakra-ui/react"

  export default function TopUpCreditForm() {
    const [amount, setAmount] = useState(0);
    const [email, setEmail] = useState(localStorage.getItem('email'));

    
	const config = {
		public_key: 'FLWPUBK-ef0188a649f945df7bee53c573aa2fbb-X',
		tx_ref: Date.now(),
		amount: amount,
		currency: 'NGN',
		payment_options: 'card,mobilemoney,ussd',
		customer: {
		  email: email,
		  phonenumber: '070',
		  name: email,
		},
		customizations: {
		  title: 'My store',
		  description: 'Payment for items in cart',
		  logo: 'https://st2.depositphotos.com/4403291/7418/v/450/depositphotos_74189661-stock-illustration-online-shop-log.jpg',
		},
	  };
	
	  const fwConfig = {
		...config,
		text: 'Proceed!',
		callback: (response) => {
      console.log("response");
      console.log(response);
		  closePaymentModal() // this will close the modal programmatically
		},
		onClose: (e) => {
      console.log("close");
      console.log(e);
    },
	  };

    const { isOpen, onOpen, onClose } = useDisclosure()
    return (
      <>
        <Button bg="#006886" border="none" onClick={onOpen}>Top Up</Button>
  
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent w="30 %" bg="#00313D" color="#fff" p="2%">
            <ModalHeader textAlign="center" mt={7}>Top Up Waller</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Text textAlign="center">Please specify amount you want to topup</Text>
             
                <FormControl mb={5}>
                  <FormLabel fontSize="xs">Amount</FormLabel>
                  <Input 
                  id="amount"
                    value={amount}
                    onChange={e => setAmount(e.target.value)}  type="number" />
                </FormControl>
								<FlutterWaveButton  {...fwConfig} />
            </ModalBody>
          </ModalContent>
        </Modal>
      </>
    )
  }
