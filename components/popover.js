import React, {useRef, useState} from "react";
import {
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverHeader,
    PopoverBody,
    PopoverFooter,
    PopoverArrow,
    PopoverCloseButton,
    PopoverAnchor,
    Button,
    Portal,
    Box,
    Flex,
    ButtonGroup
  } from '@chakra-ui/react'
  import axios from "axios";


export const PopoverComponent = (props) => {

    const [isOpen, setIsOpen] = useState(false)
  const open = () => setIsOpen(!isOpen)
  const close = () => setIsOpen(!isOpen);
  
  const deleteHandler = (teamId) => {
      props.deleteTeam(teamId)
      close()
  }

    return (
        <>
      <Popover
        returnFocusOnClose={false}
        isOpen={isOpen}
        onClose={close}
        placement='right'
        closeOnBlur={false}
      >
        <Flex className="pop-over">
         <PopoverTrigger>
           <a onClick={open}>Delete Team</a>
         </PopoverTrigger>
        </Flex>
              <PopoverContent color="black"> 
            <PopoverArrow />
                <PopoverCloseButton />
                <PopoverHeader>Confirmation</PopoverHeader>
                <PopoverBody>
                    Do you want to delete this team?                
                </PopoverBody>
                <PopoverFooter d='flex' justifyContent='flex-end'>
            <ButtonGroup size='sm'>
            <Button
                    mt={4}
                    bg="#00C1F2"
                    _hover={{ bg: "#00adf2"}}
                    onClick={close}
                  >
                    Cancel
                  </Button>
                  <Button
                    mt={4}
                    ml={3}
                    colorScheme='red'
                    onClick={() => deleteHandler(props.teamId)}
                  >
                    Delete
                  </Button>
                    </ButtonGroup>
                </PopoverFooter>
              </PopoverContent>        
      </Popover>
    </>
    )
  }