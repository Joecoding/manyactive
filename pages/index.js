import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import Header from '../components/Header';
import SideBar from '../components/SideBar';
import { Heading, Flex, Icon, Text } from '@chakra-ui/react';
import { FiFlag, FiUser, FiUsers } from 'react-icons/fi';
import WalletChart from '../components/charts/WalletChart';
import MoodDonutChart from '../components/charts/MoodDonutChart'
import MoodBarChart from '../components/charts/MoodBarChart'
import ActivityChart from '../components/charts/ActivityChart';
import { BsExclamationCircle } from 'react-icons/bs'
import { useRouter } from 'next/router';
import { applyAuth } from '../utils/auth';
import HomeChallengeCard from '../components/cards/HomeChallengeCard';
import { getTeams, getChallenges, getMembers } from './api/feedback';
import "../styles/Home.module.css";


export default function Analytics() {
  const [user, setUser] = useState({})
  const [challenges, setChallenges] = useState([])
  const [members, setMembers] = useState([])
  const [teams, setTeams] = useState([])
  const router = useRouter()
  
  useEffect(() => {
      applyAuth(router, setUser);
      getTeams(user, setTeams);
      getChallenges( user, setChallenges);
      getMembers( user, setMembers);
  }, [user, router]);
  return (
    <Flex
      h="100%"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
      justifyContent="space-between"
		>
			{/*Sidebar Column */}
			<SideBar />


      {/* Main Column */}
      <Flex
          w="85%"
          p="2%"
          flexDir="column"
          minH="100vh"
          backgroundColor="#171717"
          color="#fff"
          ml="auto"
          overscroll="scroll"
        >
          {/* Header */}
          <Header />

          <Flex flexDir="row" mt={2} justifyContent="space-between" align="center">
            <Flex flexDir="column" w="73%">
              <Flex flexDir="row" justifyContent="space-between">
                <Flex className="card" w="280px" h="fit-content" bg="#00313D" borderRadius={12} align="center">
                  <Flex flexDir="column" ml={3}>
                    <Icon borderRadius="50%" bg="#00C1F2" w="50px" h="50px" as={FiUser} />
                  </Flex>
                  <Flex flexDir="column" ml={3}>
                    <Text fontSize="32px" color="#00C1F2">{members.length}</Text>
                    <Text fontSize="sm" opacity="0.5">Members</Text>
                    <Link href="/members">View all</Link>
                  </Flex>
                </Flex>
                <Flex className="card" w="280px" h="fit-content" bg="#00313D" borderRadius={12} align="center">
                  <Flex flexDir="column" ml={3}>
                    <Icon borderRadius="50%" bg="#00C1F2" w="50px" h="50px" as={FiUsers} />
                  </Flex>
                  <Flex flexDir="column" ml={3}>
                    <Text fontSize="32px" color="#00C1F2">{teams.length}</Text>
                    <Text fontSize="sm" opacity="0.5">Teams</Text>
                    <Link href="/teams">View all</Link>
                  </Flex>
                </Flex>
                <Flex className="card" w="280px" h="fit-content" bg="#00313D" borderRadius={12} align="center">
                  <Flex flexDir="column" ml={3}>
                    <Icon borderRadius="50%" bg="#00C1F2" w="50px" h="50px" as={FiFlag} />
                  </Flex>
                  <Flex flexDir="column" ml={3}>
                    <Text fontSize="32px" color="#00C1F2">{challenges.length}</Text>
                    <Text fontSize="sm" opacity="0.5">Challenges</Text>
                    <Link href="/challenge">View all</Link>
                  </Flex>
                </Flex>
              </Flex>

              
              <Flex flexDir="column" mt={5}>
                <Flex justifyContent="space-between" align="center" mt={5}>
                    <Text>Wallet Statement</Text>
                    <Text color="teal">Generate Statement</Text>
                </Flex>
                <Flex className="wallet-chart-box" mt={2} borderRadius={10} p={2}>
                  <WalletChart />
                </Flex>
              </Flex>


              <Flex flexDir="row" justifyContent="space-between" mt={5}>
                <Flex flexDir="column" width="49%">
                  <Flex flexDir="row" align="center">
                    <Heading size="18px" fontWeight="semibold">Activity Bookings</Heading>
                    <Icon ml={3} color="#999999" as={BsExclamationCircle} />
                    <Text ml={3} color="#999999">This week</Text>
                                        
                  </Flex>
                  <Flex flexDir="row" mt={2}>
                    <ActivityChart />
                  </Flex>
                </Flex>  
                <Flex flexDir="column" width="49%">
                  <Flex flexDir="row" align="center">
                    <Heading size="18px" fontWeight="semibold">Active Percentage</Heading>
                    <Icon ml={3} color="#999999" as={BsExclamationCircle} />
                  </Flex>
                  <Flex flexDir="row" align="center">
                    <Text fontSize="30px" fontWeight="semibold">0</Text>
                    <Text fontSize="14px" ml={2} fontWeight="light" color="#999">Total</Text>
                  </Flex>
                </Flex>  
              </Flex>  
            </Flex>
            

            {/* Right Side Column */}
            <Flex flexDir="column" w="25%">
              <Flex bg="#00313D" borderRadius={10} flexDir="column">
                <Flex m={3}>
                  <Text>Chart</Text>
                </Flex>
                <Flex className="chart-box" m={5}>
                  <MoodDonutChart />
                </Flex>
                <Flex m={5}>
                <MoodBarChart />
                </Flex>
              </Flex>
            </Flex>
          </Flex>


          {/* BOTTOM ROW  */}
         <HomeChallengeCard  challenges={challenges}/>
        </Flex>
    </Flex>
  )
}
