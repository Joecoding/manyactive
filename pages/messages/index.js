import React, { useState } from 'react'
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuItemOption,
    MenuGroup,
    MenuOptionGroup,
    MenuIcon,
    MenuCommand,
    MenuDivider,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    Image
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
	FiArrowDownCircle,
	FiExternalLink,
    FiMoreVertical,
    FiEdit3
} from "react-icons/fi";

import Header from '../../components/Header';
import SideBar from '../../components/SideBar';
import ChallengeCard from '../../components/cards/ChallengeCard';
import { BiTrophy } from 'react-icons/bi';

export default function Challenge() {
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
            justifyContent="space-between"
		>
            <SideBar />
            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
                ml="auto"
			>
				{/* Header */}
				<Header />
                <Flex flexDir="row" justifyContent="space-between" h="100vh" mt={3}>
                    <Flex flexDir="column" width="75%" color="#fff">
                        <Flex justifyContent="space-between" flexDir="row" align="center">
                            <Flex>
                                <Link fontSize="sm">Upcoming</Link>
                                <Link fontSize="sm" ml={10}>Ongoing</Link>
                                <Link fontSize="sm" ml={10}>Completed</Link>
                            </Flex>
                            <Flex align="center">
                                <Flex>
                                    <Button className="button" rightIcon={<FiArrowDownCircle />} variant="ghost">Challenge Type</Button>
                                </Flex>
                                <Flex>
                                    <Button className="button" rightIcon={<FiArrowDownCircle />} variant="ghost">Date</Button>
                                </Flex>
                            </Flex>
                        </Flex>
                        <Flex flexDir="row">
                            <ChallengeCard />
                        </Flex>             
                    </Flex>
                    
                    {/* Right side column */}
                    <Flex flexDir="column" w="23%"  align="center">
                        <Box boxSize="200px">
                            <Image src="./no-challenge.png" alt="No challenge" />
                        </Box>
                        <Text>Challenge is yet to begin</Text>
                    </Flex>
                </Flex>                
            </Flex>
        </Flex>
    )
}