import React, {useState} from 'react';
import axios from 'axios';
import {
    Avatar,
    Button,
    FormControl,
    FormLabel,
    FormErrorMessage,
    FormHelperText,
    Flex,
    Heading,
    Input,
    Icon,
    Label,
    Select,
    NumberInput,
    NumberInputField,
    Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
    Lorem,
    useDisclosure,
    Radio,
    RadioGroup,
    Spacer,
    Text
  } from "@chakra-ui/react"
import { GiBootPrints, GiBoots } from 'react-icons/gi';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'; 
import moment from 'moment';

export default function NewChallengeForm({user}) {
    const { isOpen, onOpen, onClose } = useDisclosure()
    
    const [challenge, setChallenge] = useState({
        name: "",
        description: "",
        type: "",
        goal: "",
        reward: "",
        country: "",
        isPrivate: false,
        startDate: "",
        endDate: "",
        openChallenge: "",
    })
    

    const  popup = (message) => {  
      onClose();
      confirmAlert({
        title: 'Message',
        message: message,
        buttons: [
          {
            label: 'Ok',
            onClick: () => {}
          }
        ]
      });
    };

    const handleSubmit = async (e) => {
      e.preventDefault();    
      const postChallengeFormData = new FormData();   
      postChallengeFormData.append("challengeName", challenge.challengeName);
      postChallengeFormData.append("challengeDescription", challenge.challengeDescription);
      postChallengeFormData.append("challengeType", challenge.challengeType);
      postChallengeFormData.append("challengeGoal", challenge.challengeGoal);
      postChallengeFormData.append("reward", challenge.reward);
      postChallengeFormData.append("country", challenge.country);
      postChallengeFormData.append("isPrivate", challenge.isPrivate);
      postChallengeFormData.append("startDate", challenge.startDate);
      postChallengeFormData.append("endDate", challenge.endDate);
      postChallengeFormData.append("openChallenge", challenge.openChallenge);
      console.log(postChallengeFormData);
      try {
        // axios.defaults.headers.common = { Authorization: `${user}` }
        const response = await axios({
          method: "post",
          url: "https://api.manyactive.com/v1/saas/challenges",
          data: {
            name: challenge.name,
            description: challenge.description,
            type: challenge.type,
            goal: challenge.goal,
            reward: challenge.reward,
            country: challenge.country,
            isPrivate: challenge.isPrivate,
            startDate: moment(challenge.startDate).utc(),
            endDate: moment(challenge.endDate).utc(),
            openChallenge: challenge.openChallenge
          },
          headers: { 
              "Content-Type": "application/json",
              "Authorization":  `${user}`
            },
        });
        popup('Challenge successfully posted.');
        console.log(response);
      }catch(error) {
        popup('Failed to add new challenge. Please contact admin.');
        console.log(error);
      }
    };

    const handleChange = (e) => {
        e.preventDefault();
        setChallenge({
          ...challenge,
          [e.target.name]: e.target.value,
        });
      };
    
    return (
      <>
        <Button mt={5} bg="#00C1F2" border="none" onClick={onOpen}>Use Template</Button> 
        <Modal size="xl" isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <form onSubmit= {handleSubmit}>
          <ModalContent bg="#00313D" color="#fff" p="2%">
            <ModalHeader fontSize="lg" fontWeight="normal">Challenge Details</ModalHeader>
            <ModalCloseButton/>
            <ModalBody>
                <Flex flexDir="row" align="center" mb={3}>
                    <Icon borderRadius="50%" boxSize={10} bg="black" as={GiBootPrints} />
                    <Flex flexDir="column" ml={5}>
                        <Text fontSize="xs" mb={2}>Icon</Text>
                        <Flex flexDir="row">
                            <Button size="xs" bg="#00C1F2">Upload</Button>
                            <Button size="xs" variant="ghost">Use default</Button>
                        </Flex>
                    </Flex>
                </Flex>
                <Flex flexDir="row">
                    <Flex flexDir="column">
                        <FormControl mb={5}>
                            <FormLabel fontSize="xs"  htmlFor="name" fontWeight="light">Challenge Title</FormLabel>
                            <Input type="text" variant="outline" name="name"  id="name" value={challenge.name} onChange={handleChange} placeholder="eg. daily steps challenge" />                              
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="description">Description</FormLabel>
                            <Input type="text"  onChange={handleChange} value={challenge.description} id="description" name="description" placeholder="what is the challenge about" />
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="startDate">Start Date</FormLabel>
                            <Input type="datetime-local" id="startDate" name="startDate" value={challenge.startDate} onChange={handleChange} placeholder="When will the challenge start" />
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="goal">Challenge Goal</FormLabel>
                            <Input type="textarea" id="goal" name="goal" value={challenge.goal} onChange={handleChange} placeholder="what is the challenge goal" />
                        </FormControl>
                        <FormControl mb={5} as="fieldset">
                            <FormLabel fontWeight="light" fontSize="xs" htmlFor="isPrivate">Is the challenge a private challenge</FormLabel>
                            <RadioGroup  id="isPrivate"  defaultValue={false}>
                                <Flex flexDir="row">
                                    <Radio value={true} name="isPrivate" onChange={handleChange}>Yes</Radio>
                                    <Spacer />
                                    <Radio value={false} name="isPrivate" onChange={handleChange}>No</Radio>
                                </Flex>
                            </RadioGroup>
                        </FormControl>
                    </Flex>
                    <Spacer />
                    <Flex flexDir="column">
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" htmlFor="type" fontSize="xs">Challenge Type</FormLabel>
                            <Input  type="text" id="type" name="type" value={challenge.challengeType} onChange={handleChange} placeholder="eg. daily steps challenge" />
                        </FormControl>
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" htmlFor="openChallenge" fontSize="xs">Open after challenge begins?</FormLabel>
                            <Input type="text" id="openChallenge"  name="openChallenge" value={challenge.openChallenge} onChange={handleChange} placeholder="can people join after the challenge begins" />
                        </FormControl>                       
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" htmlFor="endDate" fontSize="xs">End Date</FormLabel>
                            <Input type="datetime-local" id="endDate" name="endDate" value={challenge.endDate} onChange={handleChange} placeholder="when should the challenge end" />
                        </FormControl>                       
                        <FormControl mb={5}>
                            <FormLabel fontWeight="light" htmlFor="reward" fontSize="xs">Reward</FormLabel>
                            <Input type="text" id="reward" name="reward" value={challenge.reward} onChange={handleChange} placeholder="Challenge reward" />
                        </FormControl>
                    </Flex>
 
                </Flex>
                <Flex justifyContent="space-between" mt={3}>
                    <Button onClick={onClose} variant="ghost">Cancel</Button>
                    <Button type="submit" bg="#00C1F2">Publish</Button>
                </Flex>
            </ModalBody>
          </ModalContent>
          </form>
        </Modal>
      </>
    )
  }
