import React, { useState, useEffect, useCallback} from 'react';
import axios from 'axios';
import Select from 'react-select';
import { useRouter } from 'next/router';
import {
    Avatar,
    Button,
    FormControl,
    FormLabel,
    Flex,
    Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
    useDisclosure,
    IconButton,
    Icon,
   
  } from "@chakra-ui/react"

  import {
    FiClock,
    FiPlus,
    FiMoreVertical,
  
} from "react-icons/fi";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'; 

export default function TeamsToChallengeForm(props) {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [Displayvalue,  getvalue ] = useState("");
    const [teams, setTeams] = useState({})
    const [Displayname,  setDisplay ] = useState(false);
    const [user, setUser] = useState({});
    const router = useRouter();
    
    

    const getTeams = useCallback(async () => {
        try{
          const options = [];
             const res = await axios({
              method: "get",
              url: "https://api.manyactive.com/v1/saas/teams/",
              headers: { 
                  "Authorization":   `${user}`
              },
           });
           if(!res){
              setDisplay(false)
            feedback = "No Teams Created On This Account"
          }else{
            setDisplay(true)
            for ( const key in res.data.teamsData ){
              options.push({value: key, label: res.data.teamsData[key].name, teamid: res.data.teamsData[key]._id })
             }
         setTeams(options);
          }           
      }catch(err){
          console.log(err);
      }
  }, [user])
 
    useEffect(() => {
      const authToken = localStorage.getItem('AuthToken')
      setUser(authToken || {})
      getTeams();
      if(!user){
         return router.push("/login")
      } 
    }, [user, router, getTeams])


    //popup display
    const  popup = (message) => {
      onClose();
      confirmAlert({
        title: 'Message',
        message: message,
        buttons: [
          {
            label: 'Ok',
            onClick: () => {}
          }
        ]
      });
    };
  
    
    //SubmitHandler to database api
    const handleSubmit = async (e) => {
      e.preventDefault();
       console.log(props.challengeId);
       try {
        // axios.defaults.headers.common = { Authorization: `${user}` }
        const response = await axios({
          method: "post",
          url: `https://api.manyactive.com/v1/saas/challenges/${props.challengeId}/teams`,
          data: {
           teams: Displayvalue
            },
          headers: { 
              "Content-Type": "application/json",
              "Authorization":  `${user}`
            },
        });
        popup('Teams successfully Assign.' );
        console.log(response);
      }catch(error) {
        popup('Failed to add Teams. Please contact admin.');
        console.log(error);
        console.log(Displayvalue);
      }

    }

//Select style
    const colourStyles = {
       option: (styles, {  isFocused, isSelected }) => {
        return {
        ...styles,
        backgroundColor: isFocused ? "#999999" : null,
        color : "#333333"
      };

    }
  };
  
    //OnChange Selection
    const handleChange = (e) =>
    {
      getvalue(Array.isArray(e)?e.map(x => x.teamid):[]);
     // getvaluename(Array.isArray(e)?e.map(x => x.label):[]);
      }


    return (
      <>
       <Flex>
            <Button size="sm" className="button" onClick={onOpen} leftIcon={<FiPlus />} bg="#00313D" variant="solid"> Team(s)</Button>
        </Flex>
        
        <Modal size="xl" isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <form  onSubmit = {handleSubmit} >
          <ModalContent bg="#00313D" color="#fff">
            <ModalHeader fontSize="sm" fontWeight="normal"> {Displayname ?  "Assign teams to chanllege": "Failed to add Teams. Please contact admin."  }</ModalHeader>
            <ModalCloseButton/>
            {Displayname ? 
            <ModalBody>
                 <Flex flexDir="row">
                    <FormControl mb={5}>
                            <FormLabel fontSize="sm" htmlFor="name" fontWeight="light">Teams Name</FormLabel>
                            <Select isMulti options={ teams } fontSize="lg"  styles={colourStyles} onChange = { handleChange}  />
                   </FormControl>
                </Flex>  
                <Flex justifyContent="space-between" mt={3}>
                    <Button onClick={onClose} variant="ghost">Cancel</Button>
                    <Button type="submit" bg="#00C1F2">Save</Button>
                </Flex>  
            </ModalBody> : " "
          }
          </ModalContent>
          </form>
        </Modal>  


      </>
    )
  }

 