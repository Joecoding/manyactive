import { useState } from 'react';
import React from 'react';
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    Stack
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
	FiArrowDownCircle,
    FiMoreVertical,
	FiExternalLink
} from "react-icons/fi";

import Header from '../components/Header';
import SideBar from '../components/SideBar';
import ChallengeTable from '../components/tables/ChallengeTable';
import arrowDown from "./../public/vectorarrowDown.png"
import EmployeeTable from '../components/tables/EmployeeTable';

export default function Members() {
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
            justifyContent="space-between"
		>
			{/*Sidebar Column */}
			<SideBar />
            {/* Main Column */}

            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
                ml="auto"
                overflowY="scroll"
			>
				{/* Header */}
				<Header />

				{/* Top Row */}
				<Flex justifyContent="space-between" flexDir="row" align="center">
					<Flex align="center" flexDir="row">
						<Flex>
							<Button 
                            size="xs" fontWeight="thin" 
                            className="button" 
                            // rightIcon={<FiArrowDownCircle />} 
                            variant="ghost">
                                Department
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100" />
                            </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" 
                             variant="ghost">
                                Team
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100" />
                            </Button>
						</Flex>
						<Flex>
							<Button size="xs" fontWeight="thin" ml={4} className="button" variant="ghost">
                                Country
                                <img className="arrow-down" width="15" height="15" src={arrowDown} alt="arrow-down" quality="100" />
                            </Button>
						</Flex>
                        <Flex>
                            <InputGroup borderColor="#002933" size="sm" bg="#001014" borderRadius={10}>
                                {/* <InputLeftElement
                                pointerEvents="none" children={<FiSearch color="gray.300" />}
                                /> */}
                                <Input type="text" placeholder="Search" borderRadius={10} />
                            </InputGroup>
                        </Flex>
					</Flex>
				</Flex>



                <Flex flexDir="row">
				{/* TABLE  */}				
					<EmployeeTable />				
				</Flex>
            </Flex>
		</Flex>
	)
}
