// import Button from "../components/auth/elements/button";
import Head from "next/head";
import Link from "next/link";
const siteTitle = "Manyactive";
import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
import axios from "axios";
import '../styles/Home.module.css'
import {
  Flex,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Input,
  InputGroup,
  InputRightElement,
  Checkbox,
  Button
} from "@chakra-ui/react"

import teams1 from "../public/teams1.png";
import MessageBox from "../components/auth/elements/MessageBox";

export default function Register() {
  const [formValue, setFormValue] = React.useState({
    name: "",
    email: "",
    company: "",
    city: "",
    password: "",
  });
  const [isError, setIsError] = useState(false)
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [showText, setShowText] = React.useState(false);

  const showPassword = () => setShow(!show);

  const showMe = ()  => setShowText(!showText);


  
  const hideAlert = () => {
   setAlertHidden(!alertHidden);
  };

  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const registerFormData = new FormData();
    registerFormData.append("password", formValue.name);
    registerFormData.append("email", formValue.email);
    registerFormData.append("company", formValue.company);
    registerFormData.append("city", formValue.city);
    registerFormData.append("password", formValue.password);
    registerFormData.append("email", formValue.OId);
    registerFormData.append("company", formValue.OEmail);
    registerFormData.append("city", formValue.OName);
    registerFormData.append("password", formValue.OPhone);
    registerFormData.append("password", formValue.OCountry);
    console.log(registerFormData);
    
    try {
      const response = await axios({
        method: "post",
        url: "https://api.manyactive.com/v1/saas/base/auth/register",
        data: {
          email: formValue.email,
          password: formValue.password,
          company: formValue.company,
          city: formValue.city,
          registrationSource: "Android",
        },
        headers: { "Content-Type": "application/json" },
      });
      console.log(response);

      if (response.status === 200) {
        localStorage.setItem('AuthToken', `${response.data.token}`);
        router.push("/");
      }else{
        setIsError(true)
      }
    } catch (err) {
      setIsError(true)
      setError(err.message);
      console.log('err', error);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });
  };

  return (
      <Flex
			h="100%"
			flexDir="row"
			overflow="hidden"
			maxW="2000px"
	       	>
            <Flex
                w="100%"
                p="1rem"
                flexDir="column"
                minH="50%"
				        backgroundColor="#171717"
				        color="#fff"
			>
						{/* Card Component */}
						<Flex className="card"
							w="50%"
              h="100%"
							flexDir="column"
              m='auto'
							backgroundColor="#00313D"
							p="3%"
							borderRadius={5}
						>
              {isError ?
             (<MessageBox status="error" variant="solid" alertHidden={alertHidden} isError={isError} hideAlert={hideAlert}>{error}</MessageBox>) : ""}
                          <div className="m-auto">
              <div className="text-center">
                <h2 className="primary-color">SIGN UP</h2>
              </div>
             <Flex     
                w="100%"
                p="1rem"
                flexDir="row"
                m='auto'
							  backgroundColor="#00313D"
							  p="3%"
							  borderRadius={5}
				        color="#fff"
             
             >

                  <FormControl className="mt-2 mb-1 p-3 pb-0"  >

                  <FormLabel htmlFor="email">  Do You Have Organization Id ?{" "} </FormLabel>

                  </FormControl>
                  <FormControl className="mt-2 mb-1 p-3 pb-0" >
                    Yes
                   <Checkbox onChange={showMe} /> 
                  </FormControl>
         

             </Flex>
         

              <div>
                <div className="form-body">
                  <form id="form" className="form mt-auto" onSubmit={handleSubmit}>
                  {
                   showText ? null :
                  <div>
                <FormControl isInvalid={isError} className="p-2 pb-0">
                <FormLabel htmlFor="email">Name</FormLabel>
                <Input border="2px" type="text" id="name" placeholder="name" value={formValue.name} name="name" onChange={handleChange} />
                {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
              </FormControl>
                  <FormControl isInvalid={isError}className="p-2 pb-0">
                <FormLabel htmlFor="email">Email</FormLabel>
                <Input border="2px" type="email" id="email" placeholder="email" value={formValue.email} name="email" onChange={handleChange} />
                {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
              </FormControl>
                  <FormControl isInvalid={isError} className="p-2 pb-0">
                <FormLabel htmlFor="company">Company</FormLabel>
                <Input border="2px" type="text" id="company" placeholder="company" value={formValue.company} name="company" onChange={handleChange} />
                {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
              </FormControl>
              <FormControl isInvalid={isError} className="p-2 pb-0">
                  <FormLabel htmlFor="city">City</FormLabel>
                  <Input border="2px" type="text" id="city" placeholder="city" value={formValue.city} name="city" onChange={handleChange} />
                  {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
              </FormControl>
              <FormControl isInvalid={isError} className="p-2 pb-0">
                  <FormLabel htmlFor="password">Password</FormLabel>
                  <InputGroup>
                  <Input border="2px" type={show ? "text" : "password"} id="password" placeholder="password" value={formValue.password} name="password" onChange={handleChange} />
                  {/* <FormErrorMessage>Incorrect Email</FormErrorMessage> */}
                  <InputRightElement width="4.5rem">
                  <Button colorScheme="teal" h="1.75rem" size="sm" onClick={showPassword}>
                    {show ? "Hide" : "Show"}
                  </Button>
                </InputRightElement>
                  </InputGroup>
              </FormControl>
              </div>
                   }   
            
                 {
                   showText ?
                   <div>
                   <FormControl className="mt-2 mb-1 p-3 pb-0" >
                   <FormLabel htmlFor="password">Organisation Id</FormLabel>
                   <Input border="2px" type="text" id="Organisation Id" placeholder="Organisation Id" value={formValue.OId} name="Organisation Id" onChange={handleChange} />

                   </FormControl> 

                   <FormControl className="mt-2 mb-1 p-3 pb-0" >
                   <FormLabel htmlFor="password">Organisation Email</FormLabel>
                   <Input border="2px" type="text" id="Organisation Id" placeholder="Organisation Email" value={formValue.OEmail} name="Organisation Email" onChange={handleChange} />
                   
                   </FormControl>

                   <FormControl className="mt-2 mb-1 p-3 pb-0" >
                   <FormLabel htmlFor="password">Organisation Name</FormLabel>
                   <Input border="2px" type="text" id="Organisation Id" placeholder="Organisation Name" value={formValue.OName} name="Organisation Name" onChange={handleChange} />
                   
                   </FormControl>


                   <FormControl className="mt-2 mb-1 p-3 pb-0" >
                   <FormLabel htmlFor="password">Organisation Phone</FormLabel>
                   <Input border="2px" type="text" id="Organisation Id" placeholder="Organisation Phone" value={formValue.OPhone} name="Organisation Phone" onChange={handleChange} />
                   
                   </FormControl>

                   <FormControl className="mt-2 mb-1 p-3 pb-0" >
                   <FormLabel htmlFor="password">Organisation Country</FormLabel>
                   <Input border="2px" type="text" id="Organisation Id" placeholder="Organisation Country" value={formValue.OCountry} name="Organisation Country" onChange={handleChange} />
                   
                   </FormControl>
                   
                   </div>
                   
                   
                   : null
                 }
            


              <FormControl className="mt-2 mb-1 p-3 pb-0">
              <Checkbox isRequired={true}/>
              
                I acknowledge that I have read and do hereby accept the
                terms and conditions in{" "}
                <Link href="https://manyactive.com"><a className="primary-color">manyactive</a></Link>
                {" "}Agreement and
                agree to receive valuable emails from{" "}
                <Link href="https://manyactive.com"><a className="primary-color">manyactive</a></Link>
      
             </FormControl>
          <Button 
          // isLoading 
          colorScheme="none" 
          // variant="outline" 
          type="submit" 
          className="w-100 mt-3" 
          onClick={hideAlert}  
          size="md">
          Create an account
          </Button>
          <FormControl>
              <FormHelperText>
                Already have an account?{" "}
                <Link href="/login">
                  <a className="primary-color">Sign in</a>
                </Link>
              </FormHelperText>
            </FormControl>
            </form>
              </div>
            </div>
            </div>
            </Flex>
            </Flex>
            </Flex>

  );

};


{/* <Button
  size="md"
  secondary
  className="w-100 mt-3"
  variant="primary"
  type="submit"
  onClick={isError ? hideAlert : ""}
>
  Create an account
</Button> */}
