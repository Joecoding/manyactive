import React, { useCallback, useEffect, useState, useRef} from "react";
import { applyAuth } from "../utils/auth";
import axios from "axios";
import { confirmAlert } from "react-confirm-alert";
import {
  Flex,
  Heading,
  Avatar,
  AvatarGroup,
  Text,
  Button,
  Input,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  FormControl,
  Box,
  InputGroup,
  InputLeftElement,
  useOutsideClick
} from "@chakra-ui/react";
import { FiUsers, FiSearch, FiMoreVertical, FiEdit3 } from "react-icons/fi";
import Header from "../components/Header";
import SideBar from "../components/SideBar";
import { useRouter } from "next/router";
import { WithContext as ReactTags } from "react-tag-input";
import { PopoverComponent } from "../components/popover";
import vectorTeams from "./../public/vectorTeams.png"
import menuButton from "./../public/menuButton.svg"

const KeyCodes = {
  comma: 188,
  enter: 13,
};
const delimiters = [KeyCodes.enter, KeyCodes.comma];

export default function Teams() {
  const [user, setUser] = useState({});
  const [teams, setTeams] = useState([]);
  const [teamsCount, setTeamsCount] = useState(0);
  const [formValue, setFormValue] = useState({
    name: "",
    description: "",
    organisation: "",
  });
  const [selectedId, setSelectedId] = useState("");
  const [openInviteForm, setOpenInveiteForm] = useState(false);
  const router = useRouter();
  const [tags, setTags] = useState([]);
  const ref = useRef();
  
  // useOutsideClick({
  //   ref: ref,
  //   handler: () => setOpenInveiteForm(false),
  // })

  const handleDelete = (i) => {
    setTags(tags.filter((tag, index) => index !== i));
  };

  const handleAddition = (tag) => {
    setTags([...tags, tag]);
  };
  const handleDrag = (tag, currPos, newPos) => {
    const newTags = tags.slice();
    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);
    // re-render
    setTags(newTags);
  };
  const handleTagClick = (index) => {
    console.log("The tag at index " + index + " was clicked");
  };

  //   create a new team
  const createTeam = async (e) => {
    e.preventDefault();
    const teamData = new FormData();
    teamData.append("name", formValue.name);
    teamData.append("description", formValue.description);
    teamData.append("organisation", formValue.organisation);

    try {
      const response = await axios({
        method: "post",
        url: "https://api.manyactive.com/v1/saas/teams",
        data: {
          name: formValue.name,
          description: formValue.description,
          organisation: formValue.organisation,
        },
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user}`,
        },
      });

      // update the current teams      
      const curTeams = [response.data, ...teams];
      setTeams(curTeams);
      setTeamsCount(teamsCount + 1);
    } catch (err) {
      console.log("err", err);
    }
  };

  // Handle team create form change
  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });
    console.log(formValue.organisation);
  };

  // get organisation teams
  const getTeams = useCallback(async() => {
    try {
      const response = await axios({
        method: "get",
        url: "https://api.manyactive.com/v1/saas/teams",
        headers: {
          // "Content-Type": "application/json",
          Authorization: `Bearer ${user}`,
        },
      });
      console.log("res", response.data.teamsData)
      console.log("res", response.data.count)
      setTeams(response.data.teamsData);
      setTeamsCount(response.data.count)
    } catch (err) {
      console.log(err);
    }
  }, [user]);

  // show form to add members to a team
  const showInviteForm = (id) => {
    if (selectedId !== id) {
      setTags([]);
    }
    setSelectedId(id);
    setOpenInveiteForm(true)
    console.log("id", id);
  };

  // send Invite link to members via email
  const sendInviteLink = async () => {
    try {
      // setSelectedId("");
      //      console.log("idd", selectedId);
      const response = await axios({
        method: "post",
        url: "https://api.manyactive.com/v1/saas/invite",
        data: {
          team_id: selectedId,
          email: tags,
        },
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user}`,
        },
      });
      console.log(user);
      console.log(response);
    } catch (err) {
      console.log(err);
    }
  };

  // Delete a team
  const deleteTeam = async (teamId) => {
    try {
      const response = await axios({
        method: "delete",
        url: `https://api.manyactive.com/v1/saas/teams/${teamId}`,
        headers: {
          Authorization: `${user}`,
        },
      });
      console.log(response.data);
      const findDeletedTeam = teams.find(t=>t._id === teamId);
      console.log(findDeletedTeam);
      
      let index = teams.indexOf(findDeletedTeam)
      console.log("i", index)
      let result= teams.splice(index, 1);
    if(!response.data.message){
        return;
    }

    const curTeams = [...teams]
    console.log("currentTeam", curTeams)
    console.log("deletedTeam", findDeletedTeam)
    setTeams(curTeams)
    setTeamsCount(teamsCount - 1);
    console.log(user);
    
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    applyAuth(router, setUser);
    getTeams();
  }, [user, router, getTeams]);

  return (
    <Flex h="100%" flexDir="row" overflow="hidden" maxW="100%" justifyContent="space-between">
      {/*Sidebar Column */}
      <SideBar/>
      {/* Main Column */}

      <Flex
        w="85%"
        p="2%"
        flexDir="column"
        minH="100vh"
        backgroundColor="#171717"
        color="#fff"
        ml="auto"
        // overflowY="scroll"
        >
        {/* Header */}
        <Header />
        {/* Top Row */}
        {/* <Flex flexDir="row" justifyContent="space-between" align="center">

        </Flex> */}
        <Flex flexDir="row" justifyContent="space-between" h="100%" mt={3}>
          <Flex flexDir="column" width="75%" color="#fff"  pr={2}>
          <Flex justifyContent="space-between" width="40%" pl={3}>
            <Text>Teams</Text>
            <Flex>
                            <InputGroup borderColor="#002933" size="sm" bg="#001014" borderRadius={10}>
                                <InputLeftElement pointerEvents="none">
                                <FiSearch color="gray.300"/>
                                </InputLeftElement>
                                <Input type="text" placeholder="Search" borderRadius={10} />
                            </InputGroup>
                        </Flex>
          </Flex>
          {/* <Flex>
            
          </Flex> */}
            {teams ? (
              teams.map((team) => {
                return (
                  <Box key={team._id}>
                    <Flex                      
                      flexDir="row"
                      justifyContent="space-between"
                      align="center"
                      p={3}
                      backgroundColor="#00313D"
                      borderRadius={5}
                      m={3}
                    >
                      <Flex flexDir="column" m={3}>
                        <Heading size="sm" fontWeight="normal">
                          {team.description}
                        </Heading>
                        <Text color="#00C1F2">{team.members? team.members.length: 0} Members</Text>
                      </Flex>
                      <Flex flexDir="column">
                        <AvatarGroup size="md" max={10}>
                          <Avatar
                            name="Ryan Florence"
                            src="https://bit.ly/ryan-florence"
                          />
                          <Avatar
                            name="Segun Adebayo"
                            src="https://bit.ly/sage-adebayo"
                          />
                          <Avatar
                            name="Kent Dodds"
                            src="https://bit.ly/kent-c-dodds"
                          />
                          <Avatar
                            name="Prosper Otemuyiwa"
                            src="https://bit.ly/prosper-baba"
                          />
                          <Avatar
                            name="Christian Nwamba"
                            src="https://bit.ly/code-beast"
                          />
                          <Avatar
                            name="Ryan Florence"
                            src="https://bit.ly/ryan-florence"
                          />
                          <Avatar
                            name="Segun Adebayo"
                            src="https://bit.ly/sage-adebayo"
                          />
                          <Avatar
                            name="Kent Dodds"
                            src="https://bit.ly/kent-c-dodds"
                          />
                          <Avatar
                            name="Prosper Otemuyiwa"
                            src="https://bit.ly/prosper-baba"
                          />
                          <Avatar
                            name="Christian Nwamba"
                            src="https://bit.ly/code-beast"
                          />
                          <Avatar
                            name="Ryan Florence"
                            src="https://bit.ly/ryan-florence"
                          />
                          <Avatar
                            name="Segun Adebayo"
                            src="https://bit.ly/sage-adebayo"
                          />
                          <Avatar
                            name="Kent Dodds"
                            src="https://bit.ly/kent-c-dodds"
                          />
                          <Avatar
                            name="Prosper Otemuyiwa"
                            src="https://bit.ly/prosper-baba"
                          />
                          <Avatar
                            name="Christian Nwamba"
                            src="https://bit.ly/code-beast"
                          />
                        </AvatarGroup>
                      </Flex>
                      <Flex>
                        <Menu isLazy bg="black">
                          <MenuButton
                            variant="unstyled"
                            as={Button}
                            icon={<FiMoreVertical />}
                            onClick={() => setOpenInveiteForm(false)}
                          >
                            <img className="menu-button" width="20" height="15" src={menuButton} alt="arrow-down" quality="100" />
                            </MenuButton>
                          <MenuList outline="none" bg="#171717" color="#fff">
                            <MenuItem icon={<FiEdit3 />}>Edit</MenuItem>
                            <MenuItem onClick={() => showInviteForm(team._id)}>
                              Invite
                            </MenuItem>
                            <MenuItem>Export</MenuItem>
                            <PopoverComponent
                              deleteTeam={deleteTeam}
                              teamId={team._id}
                              
                            />
                          </MenuList>
                        </Menu>
                      </Flex>
                    </Flex>
                    {team._id === selectedId && openInviteForm && (
                      <Flex
                        key={team._id}
                        flexDir="row"
                        justifyContent="space-between"
                        mt={5}
                        align="center"
                        borderRadius={5}
                        // ref={ref}
                      >
                          <Flex flexDir="column">
                          <ReactTags                          
                            tags={tags}
                            inputFieldPosition="inline"
                            // suggestions={suggestions}
                             delimiters={delimiters}
                            handleDelete={handleDelete}
                            handleAddition={handleAddition}
                            handleDrag={handleDrag}
                            handleTagClick={handleTagClick}
                            classNames={{
                              tags: "ReactTags__tags",
                              tagInput: "ReactTags__tagInput",
                              tagInputField: "ReactTags__tagInputField",
                              selected: "ReactTags__selected",
                              tag: "ReactTags__tag",
                              remove: "ReactTags__remove",
                              suggestions: "ReactTags__suggestions",
                              activeSuggestion: "ReactTags__activeSuggestion",
                              editTagInput: "ReactTags__editInput",
                              editTagInputField: "ReactTags__editTagInput",
                              clearAll: "ReactTags__clearAll",
                            }}
                            autocomplete
                            labelField={"email"}
                          />
                      </Flex>
                        <Flex flexDir="column">
                          <Button bg="#00C1F2" _hover={{ bg: "#00adf2"}} onClick={sendInviteLink} mr={3}>Invite</Button>
                        </Flex>
                      </Flex>
                    )}
                  </Box>
                );
              })
            ) : (
              <Flex> </Flex>
            )}
          </Flex>
          {/* Right side column */}
          <Flex 
          flexDir="column" 
          w="30%"
           pl={1}
           mt={12}
           >
            <Flex
              backgroundColor="#00313D"
              borderRadius={5}
              pb={3}
              flexDir="column"
              alignItems="center"
              justifyContent="center"
            >
              <Heading fontWeight="normal" size="md" m={3}>
                Create Team
              </Heading>

              <Flex mt={3}>
                <form onSubmit={createTeam}>
                  <FormControl>
                    <Input
                      mb={4}
                      type="text"
                      variant="outline"
                      onChange={handleChange}
                      name="organisation"
                      value={formValue.organisation}
                      placeholder="Many Active"
                    />
                  </FormControl>
                  <FormControl>
                    <Input
                      mb={4}
                      type="text"
                      variant="outline"
                      onChange={handleChange}
                      name="description"
                      value={formValue.description}
                      placeholder="Description"
                    />
                  </FormControl>
                  <FormControl>
                    <Input
                      mb={4}
                      type="text"
                      variant="outline"
                      onChange={handleChange}
                      name="name"
                      value={formValue.name}
                      placeholder="organisation"
                    />
                  </FormControl>
                  <Flex justifyContent="space-between" mt={3}>
                    <Button variant="ghost">Cancel</Button>
                    <Button type="submit" bg="#00C1F2" _hover={{ bg: "#00adf2"}} mr={4} flexWrap="wrap" p={2}>
                      Create Team
                    </Button>
                  </Flex>
                </form>
              </Flex>
            </Flex>
            <Flex
              mt={3}
              backgroundColor="#00313D"
              borderRadius={5}
              p={6}
              flexDir="column"
            >
              <Flex flexDir="row" justifyContent="space-between">
                <Flex align="center" ml={4}>
                <img className="vector-teams" width="15" height="15" src={vectorTeams} alt="arrow-down" quality="100"  />
                  <Text>Teams</Text>
                </Flex>
                <Flex align="center">
                <img className="vector-teams" width="20" height="20" src={vectorTeams} alt="arrow-down" quality="100"  />
                  <Text>Members</Text>
                </Flex>
              </Flex>
              <Flex flexDir="row" justifyContent="space-between" px={10}>
                <Text fontSize="4xl">{teamsCount}</Text>
                <Text fontSize="4xl">50</Text>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
}

