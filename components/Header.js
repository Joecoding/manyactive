import React from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import NewUserForm from './forms/NewUserForm';

import {
    Image,
    Flex,
    Heading,
    Avatar,
    Text,
    Icon,
    IconButton,
    Button,
    Select,
} from '@chakra-ui/react'
import {
    FiUser,
    FiBell,
    FiUserPlus
} from "react-icons/fi";
import { FaRunning } from 'react-icons/fa';
import vectorBell from "./../public/vectorBell.png";

export default function Header() {
    const router = useRouter();
    return(
        <Flex
            flexDir="column"
            >
                {router.pathname == "/" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={1}>
                        <Flex flexDir="column">
                            <Flex align="center">
                                <Text fontWeight="light" fontSize="16px">Home</Text> 
                                <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                            </Flex>
                            <Heading fontWeight="normal" fontSize="20px">Analytics</Heading>
                            
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                        
                            <Flex  ml={4} className="bell">
                            <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="15" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                                {/* <Flex  className="vector-bell" ml={4} borderRadius="50%">
                            </Flex> */}
                                <Flex
                                    w={11}
                                    h={11}
                                    bgColor="#FF503F"
                                    borderRadius="50%"
                                    color="#fff"
                                    align="center"
                                    justify="center"
                                    ml="-4"
                                    mt="1"
                                    zIndex="100"
                                    boxSize="7px"></Flex>
                            </Flex>
                            <Flex ml={4}>
                               
                                <Select icon={ <Avatar ml={4} className="avatar" size="md" src="./avatar.png" />}  onClick={() =>  { }}   placeholder="Logout"  bg='#00C1F2' />
                                
                            </Flex>
                        </Flex>
                    </Flex> : "" }
                    {router.pathname == "/members" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={2}>
                        <Flex flexDir="column">
                            <Flex align="center">
                                <Text fontWeight="light" fontSize="16px">Home</Text> 
                                <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                            </Flex>
                            <Heading fontWeight="normal" fontSize="20px">Members</Heading>
                            
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                            <Flex>
                                <Button size="sm" className="button" leftIcon={<FiUser />} bg="#00C1F2" variant="solid">Add Member(s)</Button>
                            </Flex>
                            <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="18" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={4} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }

                    {router.pathname == "/wallet" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={2}>
                        <Flex flexDir="column">
                            <Flex align="center">
                                <Text fontWeight="light" fontSize="16px">Home</Text> 
                                <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                            </Flex>
                            <Heading fontWeight="normal" fontSize="20px">Wallet</Heading>
                            
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                            <Flex>
                                {/* <Button size="sm" className="button" leftIcon={<FiUserPlus />} bg="#00C1F2" variant="solid">
                                    Add User(s)
                                    </Button> */}
                                    <NewUserForm  />
                            </Flex>
                            <Flex  className="vector-bell" ml={5} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="18" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={5} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }

                    {router.pathname == "/challenge" || router.pathname == "/challenge/new" || router.pathname == "/challenge/upcoming" || router.pathname == "/challenge/ongoing" || router.pathname == "/challenge/completed" ? 
                    <Flex flexDir="row" justifyContent="space-between" 
                    mb={2}>
                        <Flex flexDir="row" align="center">
                            <Flex flexDir="column">
                                <Flex align="center">
                                    <Text fontWeight="light" fontSize="16px">Home</Text> 
                                    <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                                </Flex>
                                <Heading fontWeight="normal" fontSize="20px">Challenge</Heading>
                                
                            </Flex>
                            <Flex flexDir="column">
                                {router.pathname != "/challenge/new" ? 
                                    <Link href="/challenge/new" passHref>
                    
                                        <Button ml={3} to="/challenge/new" size="sm" bg="#00C1F2" leftIcon={<FaRunning />}>Start a New Challenge</Button>
                                    </Link>
                             : ""}
                            </Flex>
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                        <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="15" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={5} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }

                    {router.pathname == "/teams" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={2}>
                        <Flex flexDir="row" align="center">
                            <Flex flexDir="column">
                                <Flex align="center">
                                    <Text fontWeight="light" fontSize="16px">Home</Text> 
                                    <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                                </Flex>
                                <Heading fontWeight="normal" fontSize="20px">Teams</Heading>
                                
                            </Flex>
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                        <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="15" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={5} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }

                    {router.pathname == "/leaderboard" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={2}>
                        <Flex flexDir="row" align="center">
                            <Flex flexDir="column">
                                <Flex align="center">
                                    <Text fontWeight="light" fontSize="16px">Home</Text> 
                                    <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                                </Flex>
                                <Heading fontWeight="normal" fontSize="20px">Leaderboard</Heading>
                                
                            </Flex>
                            <Flex flexDir="column">
                                <Link href="/challenge/new" passHref>
                        
                                    <Button ml={3} size="sm" bg="#00C1F2" leftIcon={<FaRunning />}>Start a New Challenge</Button>
                                </Link>
                            </Flex>
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                            <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="15" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={5} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }

                    {router.pathname == "/moodboard" ? 
                    <Flex flexDir="row" justifyContent="space-between"
                    mb={2}>
                        <Flex flexDir="column">
                            <Flex align="center">
                                <Text fontWeight="light" fontSize="16px">Home</Text> 
                                <Image  ml={3} src="/cocacola.png" alt="#" width={68} height={15} />
                            </Flex>
                            <Heading fontWeight="normal" fontSize="20px">Moodboard</Heading>
                            
                        </Flex>
                        

                        <Flex flexDir="row" align="center" mt={0}>
                            <Flex>
                                <Button size="sm" className="button" leftIcon={<FiUserPlus />} bg="#00C1F2" variant="solid">Add User(s)</Button>
                            </Flex>
                            <Flex  className="vector-bell" ml={4} borderRadius="50%">
                                <img  bg="#063041" borderRadius="50%" color="#00C1F2" width="18" height="15" src={vectorBell} alt="arrow-down" quality="100" />
                            </Flex>
                            <Flex ml={4}>
                                <Avatar ml={5} className="avatar" size="md" src="./avatar.png" />
                            </Flex>
                        </Flex>
                    </Flex> : "" }
        </Flex>
    )
}