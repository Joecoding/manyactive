import React from 'react';
import {
    Image,
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
    Stack,
    ChakraProvider,
    NumberInputField
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell
} from "react-icons/fi";


export default function ChallengeTable() {
    return(
        <Flex flexDir="column" w="100%">
            <Flex overflow="auto">
                <Table variant="unstyled" mt={4}>
                    <Thead>
                        <Tr backgroundColor="#00313D">
                            <Th>Ranking</Th>
                            <Th isNumeric>Steps</Th>
                            <Th>City</Th>
                            <Th>All Teams</Th>
                            <Th>Last updated</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        <Tr>
                            <Td>
                                <Flex>
                                    <Flex>
                                        <Text>1</Text>
                                    </Flex>
                                    <Avatar size="sm" mr={2} />
                                    <Flex>
                                        <Text>Akinoluwa Adeleye</Text>
                                    </Flex>
                                </Flex>
                            </Td>
                            <Td isNumeric>20000</Td>
                            <Td>Lagos</Td>
                            <Td>HR</Td>
                            <Td>20mins ago</Td>
                        </Tr>
                        <Tr>
                            <Td>
                                <Flex>
                                    <Flex>
                                        <Text>2</Text>
                                    </Flex>
                                    <Avatar size="sm" mr={2} />
                                    <Flex>
                                        <Text>Akinoluwa Adeleye</Text>
                                    </Flex>
                                </Flex>
                            </Td>
                            <Td isNumeric>15000</Td>
                            <Td>Abuja</Td>
                            <Td>HR</Td>
                            <Td>20mins ago</Td>
                        </Tr>
                        <Tr>
                            <Td>
                                <Flex>
                                    <Flex>
                                        <Text>3</Text>
                                    </Flex>
                                    <Avatar size="sm" mr={2} />
                                    <Flex>
                                        <Text>Akinoluwa Adeleye</Text>
                                    </Flex>
                                </Flex>
                            </Td>
                            <Td isNumeric>13000</Td>
                            <Td>Lagos</Td>
                            <Td>Marketing</Td>
                            <Td>20mins ago</Td>
                        </Tr>
                        <Tr>
                            <Td>
                                <Flex>
                                    <Flex>
                                        <Text>4</Text>
                                    </Flex>
                                    <Avatar size="sm" mr={2} />
                                    <Flex>
                                        <Text>Akinoluwa Adeleye</Text>
                                    </Flex>
                                </Flex>
                            </Td>
                            <Td isNumeric>8000</Td>
                            <Td>Abuja</Td>
                            <Td>HR</Td>
                            <Td>20mins ago</Td>
                        </Tr>
                        
                    </Tbody>
                </Table>
            </Flex>
        </Flex>
        
    )
}