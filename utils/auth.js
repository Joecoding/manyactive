import axios from "axios";

const verifyAuth = async (token) => {
  try {
        return (await axios({
          method: "get",
          url: "https://api.manyactive.com/v1/users/profile",
          headers: {
            // "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        })).status == 200;
      } catch(err) {
        // meaning profile request is not successful
        return false;
      }
        
}


export const applyAuth = async (router, setUser) => {

  console.log("applyAuth function working.")
; 
        const authToken = localStorage.getItem('AuthToken');
         if(authToken && await verifyAuth(authToken)) {
          // check for token validity
          return setUser(authToken);
          // checlkl
        }

        return router.push("/login")
        

        

 

};
