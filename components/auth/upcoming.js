import React, { useState } from 'react'
import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuItemOption,
    MenuGroup,
    MenuOptionGroup,
    MenuIcon,
    MenuCommand,
    MenuDivider,
    Button,
    Input,
    InputGroup,
    InputLeftElement,
	HStack,
    Image
} from '@chakra-ui/react'
import {
    FiHome,
    FiUser,
    FiUsers,
    FiBarChart2,
	FiBarChart,
    FiFlag,
    FiTrendingUp,
    FiSmile,
    FiSettings,
    FiLogOut,
    FiPieChart,
    FiDollarSign,
    FiBox,
    FiCalendar,
    FiChevronDown,
    FiChevronUp,
    FiPlus,
    FiCreditCard,
    FiSearch,
    FiBell,
	FiArrowDownCircle,
	FiExternalLink,
    FiMoreVertical,
    FiEdit3
} from "react-icons/fi";

import Header from '../Header';
import SideBar from '../SideBar';
import ChallengeCard from '../cards/ChallengeCard';
import { BiTrophy } from 'react-icons/bi';
import { GiBootPrints } from 'react-icons/gi';
import NewChallengeForm from '../forms/NewChallengeForm';

export default function NewChallenge() {
	return (
		<Flex
			h="100vh"
			flexDir="row"
			overflow="hidden"
			maxW="2000px"
		>
            <SideBar />

            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="50vh"
				backgroundColor="#171717"
				color="#fff"
			>
				{/* Header */}
				<Header />
                

                <Flex flexDir="column" h="100vh" mt={3}>
                    <Flex flexDir="row">
                        <Heading as="h2" size="md" fontWeight="semibold">Pre-made Challenges</Heading>
                    </Flex>
                    <Flex flexDir="row" justifyContent="space-between" mt={5}>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">Steps Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <NewChallengeForm />
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">Steps Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <NewChallengeForm />
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">Steps Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <NewChallengeForm />
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">Steps Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <NewChallengeForm />
                        </Flex>
                    </Flex>
                    <Flex flexDir="row" mt={2}>
                        <Heading as="h2" size="md" fontWeight="semibold">Created Challenges</Heading>
                    </Flex>
                    <Flex flexDir="row" justifyContent="space-between" mt={5}>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">HR Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                                Publish
                            </Button>
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">HR Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                                Publish
                            </Button>
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">HR Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                                Publish
                            </Button>
                        </Flex>
                        <Flex className="card" flexDir="column" bg='#00313D' borderRadius={5} h="260px" w="260px" p={5}>
                            <Icon fontSize="2xl" color="#00C1F2" mb={4} as={GiBootPrints} />
                            <Heading mb={4} size="md" fontWeight="normal">HR Challenge</Heading>
                            <Text fontSize="sm" fontWeight="thin">This challenge helps participants see how many steps they take during the challenge period</Text>
                            <Button mt={5} w="100%" type="submit" bg="#00C1F2">
                                Publish
                            </Button>
                        </Flex>
                    </Flex>
                </Flex>

                
            </Flex>
        </Flex>
    )
}