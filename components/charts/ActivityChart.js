import React from 'react';
import { Doughnut } from 'react-chartjs-2';

const data = {
  labels: ['Trainers', 'Wellness', 'Gyms', 'Classes'],
  datasets: [
    {
      data: [0, 0, 0, 0],
      backgroundColor: ["#00C1F2", "#123943", "#0E5B6F", "#0696BC"],
      borderColor: "#00C1F2",
      borderWidth: 0,
      borderRadius: Number.MAX_VALUE,
      borderSkipped: false,
      cutout: '80%',
      scaleShowLabels: false, 
    },
  ],
};






const options = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
      legend: {
          display: false,
          position: 'right',
          
      }
  },
  scaleShowLabels: false
};


const plugins = [{
  beforeDraw: function(chart) {
   var width = chart.width,
       height = chart.height,
       ctx = chart.ctx;
       ctx.restore();
       var fontSize = (height / 160).toFixed(2);
       ctx.font = fontSize + "em sans-serif";
       ctx.textBaseline = "top";
       var text = "0 Activity",
       textX = Math.round((width - ctx.measureText(text).width) / 2),
       textY = height / 2;
       ctx.fillText(text, textX, textY);
       ctx.save();
  } 
}]




const ActivityChart = () => (
    <Doughnut 
        data={data}
        options={options}
        plugins={plugins}
    />
)


export default ActivityChart;