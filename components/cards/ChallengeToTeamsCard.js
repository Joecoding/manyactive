import React, { useEffect, useState } from "react";

import { Flex, 
    Heading, 
    Text, 
    Icon,
    Button,
    Box, 
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    IconButton,
 } from "@chakra-ui/react";
import { useRouter } from "next/router";

import Header from "../../components/Header";
import SideBar from "../../components/SideBar";
import { GiBootPrints } from "react-icons/gi";
import NewChallengeForm from "../../components/forms/NewChallengeForm";
import TeamsToChallengeForm from '../forms/TeamsToChallengeForm';
import axios from "axios";
import {
    FiClock,
    FiUsers,
    FiMoreVertical,
  
} from "react-icons/fi";

export default function ChallengeToTeams(props) {
  const [user, setUser] = useState({})
  const router = useRouter()

  useEffect(() => {   
    applyAuth(router, user, setUser);
  }, [user, router])

  

  


  return (
    <Flex h="100%" flexDir="row" overflow="hidden" maxW="2000px">
    
      <Flex
        w="85%"
        p="2%"
        flexDir="column"
        minH="50vh"
        backgroundColor="#171717"
        color="#fff"
      >
        
        
    
          <Flex flexDir="row" mt={2}>             
            <Heading as="h2" size="md" fontWeight="semibold">
              Ongoing Challenges
            </Heading>
          </Flex>
          <Flex flexDir="row" justifyContent="space-between" mt={5}>
          <Box
                        borderRadius="lg"
                        maxW="sm"
                        bg='#00313D'
                        p={2}
                        m={3}
                        w="100%"
                        >
                        <Flex flexDir="row"  justifyContent="flex-end" >
                       
                                  <TeamsToChallengeForm challengeId='ururu'/> 
                            
                            </Flex>
                        <Flex flexDir="column" mx={2}>
                            <Text mb={2} fontWeight="thin" fontSize="xs">Time</Text>
                            <Flex mb={3} align="center">
                                <Icon size="lg" color="#00C1F2" m={2} as={GiBootPrints} />
                                <Heading fontWeight="normal" letterSpacing="tight" ml={2} size="md">Name</Heading>
                            </Flex>
                            <Flex mb={5} flexDir="column">
                                <Heading fontWeight="normal" as="h5" size="xs">Description</Heading>
                                <Text as="p" fontSize="xs" letterSpacing="tight" fontWeight="thin">Description</Text>
                            </Flex>
                            <Flex mb={5} flexDir="row">
                                <Flex flexDir="column">
                                    <Heading fontWeight="normal" fontSize="xs">Challenge Type</Heading>
                                    <Text fontWeight="thin" fontSize="xs" letterSpacing="tight">Type</Text>
                                </Flex>
                                
                                <Flex flexDir="column" ml={4}>
                                    <Heading fontWeight="normal" fontSize="xs">Challenge ID</Heading>
                                    <Text fontWeight="thin" fontSize="xs" letterSpacing="tight">ID</Text>
                                </Flex>
                            </Flex>
                            <Flex align="center">
                                <Icon color="#34A853" as={FiClock} />
                                <Text fontSize="xs" ml={1}>Ongoing</Text>
                            </Flex>

                        </Flex>
                    </Box>
         
          
          </Flex>
        </Flex>
      </Flex>
    
  );
}



  
