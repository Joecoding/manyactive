import React from 'react';

import {
    Flex,
    Heading,
    Avatar,
    AvatarGroup,
    Text,
    Icon,
    IconButton,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    Divider,
    Link,
    Box,
    Grid,
    GridItem,
} from '@chakra-ui/react'
import {
    FiMoreVertical,
    FiPlusCircle,
    FiClock
} from "react-icons/fi";

import { GiBootPrints } from 'react-icons/gi'
import moment from 'moment';
import MessageBox from '../auth/elements/MessageBox';
import TeamsToChallengeForm from '../forms/TeamsToChallengeForm';
import ChallengeToTeams from '../../components/cards/ChallengeToTeamsCard';

export default function HomeChallengeCard(props) {
    //Teams object

    console.log(props.challenges)
   
    return(

        <Flex flexDir="column" mt={5}>
           <Flex flexDir="row" justifyContent="space-between">
              <Heading size="sm" fontWeight="semibold">Challenges</Heading>
              <Link href="/challenge">View all</Link>
            </Flex>
                <Flex flexDir="row" justifyContent="space-between" mt={5}>
                    {
                        props.challenges ? 
                        
                        (props.challenges.slice(0, 6).map((doc, i) => {
                    return( 
                 
                        // <Box
                        // borderRadius="lg"
                        // maxW="sm"
                        // bg='#00313D'
                        // p={2}
                        // m={3}
                        // w="100%"
                        // key={doc._id}>
                        
                 <Flex flexDir="column" key={doc._id}  bg="#00313D" borderRadius={10} w="144px" h="208px" p={4}>
                <Flex flexdir="row">
                  <Icon fontSize="3xl" color="#00C1F2" as={GiBootPrints} />
                </Flex>
                <Heading fontSize="15px" color="#00C1F2" fontWeight="normal" mt={4}>{doc.goal}</Heading>
                <Text fontWeight="light" color="#00C1F2" fontSize="12px" mt={3}>{doc.description}</Text>
                <Flex mt={3} flexDir="row" align="center">
                  <AvatarGroup size="xs" max={2}>
                    <Avatar name="Ryan Florence" src="https://bit.ly/ryan-florence" />
                    <Avatar name="Segun Adebayo" src="https://bit.ly/sage-adebayo" />
                    <Avatar name="Kent Dodds" src="https://bit.ly/kent-c-dodds" />
                    <Avatar name="Prosper Otemuyiwa" src="https://bit.ly/prosper-baba" />
                    <Avatar name="Christian Nwamba" src="https://bit.ly/code-beast" />
                  </AvatarGroup>
                  <Icon fontSize="lg" ml={2} fill="#fff" color="#00313D" opacity="0.7" as={FiPlusCircle} />
                </Flex>
              </Flex>
             
 

                    // </Box>

                        ) })) : ""
                    
                            } 

            <Flex flexDir="column" bg="#00313D" borderRadius={10} w="144px" h="208px" p={4} align="center">      
              <Icon fontSize="lg" ml={2} fill="#fff" color="#00313D" opacity="0.7" as={FiPlusCircle} />
            </Flex>
                </Flex>
        </Flex>


    )
}
{/* <ChallengeToTeams /> */}