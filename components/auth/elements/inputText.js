import React from "react";

export default function Input({ secondary, className, ...props }) {
    let secondaryProp = secondary ? "secondary" : null;
    return (
        <input type="text"
            {...props}
            className={`${secondaryProp}  ${className ? className : ""} input`}
        />
    );
}
