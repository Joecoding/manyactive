import React, { useState, useEffect, useCallback } from 'react'
import { useRouter } from "next/router";
import Link from 'next/link';
import cookieCutter from 'cookie-cutter';


import axios from 'axios';
import {
    Flex,
    Text,
    Box,
    Button,
    Image
} from '@chakra-ui/react'
import {
	FiArrowDownCircle,
} from "react-icons/fi";

import Header from '../../components/Header';
import SideBar from '../../components/SideBar';
import ChallengeCard from '../../components/cards/ChallengeCard';
import { BiTrophy } from 'react-icons/bi';
import { applyAuth } from '../../utils/auth';
import { getChallenges } from '../api/feedback';
import trophyImage from "./../../public/emojione_trophy.png"
import arrowDown from "./../../public/vectorarrowDown.png";
import noChallenge from "./../../public/no-challenge.png";

export default function UpcomingChallenge({ teams }) {
    const [user, setUser] = useState({})
    const [challenges, setChallenges] = useState([])
    const router = useRouter()
    const [error, setError] = useState("")


    const getUpcomingChallenges = useCallback(async () => {    
        try {
          // axios.defaults.headers.common = { Authorization: `${user}` }
          const response = await axios({
            method: "get",
            url: "https://api.manyactive.com/v1/saas/challenges/foruser?status=upcoming",
            headers: { 
                // "Content-Type": "application/json",
                "Authorization":  `Bearer ${user}`
              },
          });
          console.log(response)
          setChallenges(response.data);
 
        }catch(err) {
          setError(err)
          console.log('err', error);
        };
      }, [user, error]);
    
    useEffect(() => {
        applyAuth(router, setUser);
        getUpcomingChallenges();
      }, [user, router, getUpcomingChallenges])

	return (
		<Flex
			h="100%"
			flexDir="row"
			overflow="hidden"
			maxW="100%"
            justifyContent="space-between"
		>
            <SideBar />

            <Flex
                w="85%"
                p="2%"
                flexDir="column"
                minH="100vh"
				backgroundColor="#171717"
				color="#fff"
                ml="auto"
			>
				{/* Header */}
				<Header />
                <Flex flexDir="row" justifyContent="space-between" h="100%" mt={3}>
                    <Flex flexDir="column" width="75%" color="#fff">
                        <Flex justifyContent="space-between" flexDir="row" align="center">
                            <Flex fontSize="sm" width="30%" justifyContent="space-between">
                            <Link href="/challenge" fontSize="sm" mr={10}>
                                <a>
                                    Upcoming

                                </a>                                
                            </Link>
                                <Link href="/challenge/ongoing"  fontSize="sm" ml={10}>
                                <a>
                                    Ongoing
                                </a>
                                </Link>
                                <Link href="/challenge/completed" fontSize="sm" ml={10}>
                                <a>
                                Completed
                                </a>
                            </Link>
                            </Flex>
                            <Flex align="center">
                                <Flex>
                                    <Button 
                                    className="button" 
                                    variant="ghost">
                                        Challenge Type
                                        <img className="arrow-down" width="15" height="10" src={arrowDown} alt="arrow-down" quality="100" />
                                    </Button>
                                </Flex>
                                <Flex>
                                    <Button className="button" variant="ghost">
                                        Date
                                        <img className="arrow-down" width="15" height="10" src={arrowDown} alt="arrow-down" quality="100" />
                                    </Button>
                                </Flex>
                            </Flex>
                        </Flex>
                        <Flex flexDir="row">
                            <ChallengeCard challenges = {challenges}  />
                        </Flex>             
                    </Flex>
                    
                    {/* Right side column */}
                    <Flex flexDir="column" w="23%" align="center">
            {/* <Box boxSize="200px">
                            <Image src="./no-challenge.png" alt="No challenge" />
                        </Box>
                        <Text>Challenge is yet to begin</Text> */}
            <Flex
              justifyContent="space-between"
              flexDir="row"
              align="center"
              w="100%"
            >
              <Flex mt={3}>
                <Text fontSize="sm">Leaderboard</Text>
              </Flex>
              <Flex align="center">
                <Link href="#">View all</Link>
              </Flex>
            </Flex>  
            <Flex flexDir="column" mt={10}>

           <Box boxSize="150px">
                <img src={noChallenge} alt="No challenge" />
            </Box>
            <Text>Challenge is yet to begin</Text>
            </Flex>        
          </Flex>
                </Flex>

                
            </Flex>
        </Flex>
    )
}



