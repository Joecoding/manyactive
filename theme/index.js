import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
  fonts: {
    heading: 'Open Sans',
    //body: 'Raleway',
    body: 'Cursive',
  },
})

export default theme